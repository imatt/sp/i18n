* Integração com Territory Helper

* Novo layout do ecrã Hoje
  - Escolha que Plugins pretende ativar
  - Controle a ordem dos Plugins
  - Plugin de Nova Atividade 
  - Novo Plugin do Resumo das Horas

* Suporte para multiplos parceiros de testemunho
  - Com integração no Calendário do iOS

* Melhor integração com o Google Maps

* Melhorias na reprodução dos vídeos

* Possibilidade de adicionar 2 números de telefone por contato

* Possibilidade de apagar Itens colocados, Videos, Revisitas do relatório (mas manter no registo do contacto)

* Várias melhorias no interface do usuário e vários problemas resolvidos

Incentivo-o a ler sobre estes novos recursos no site:
https://serviceplanner.me/blog/

---

Avaliações positivas na App Store são incrivelmente úteis. Se está a gostar do ServicePlanner, e se sentir inspirado para deixar um comentário, seria muito apreciado. Obrigado pelo seu apoio.

Website: www.serviceplanner.me
Documentos de Ajuda: www.serviceplanner.me/help
Registo de Problemas: www.serviceplanner.me/issues
Facebook: www.facebook.com/ServicePlannerApp
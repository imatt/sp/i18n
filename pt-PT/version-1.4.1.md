Esta é uma pequena atualização com apenas um novo recurso...

No separador das Visitas agora tem uma opção para partilhar o contacto.

É uma funcionalidade bastante básica de momento. Envia um e-mail com os detalhes do Contacto com todo o histórico das Visitas efetuadas. De momento não existe a opção para outro utilizador de ServicePlanner poder importar o contacto. Será uma opção futura. O foco no momento é fornecer um método básico de partilhar os detalhes de contato com outro publicador. Para este botão funcionar deve ter o seu equipamento configurado para enviar e-mails pela aplicação padrão "Mail".

Até a próxima, desfrute do ServicePlanner!

---

Avaliações positivas na App Store são incrivelmente úteis. Se está a gostar do ServicePlanner, e se sentir inspirado para deixar um comentário, seria muito apreciado. Obrigado pelo seu apoio.

Website: www.serviceplanner.me  
Documentos de Ajuda: www.serviceplanner.me/help  
Registo de Problemas: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp 
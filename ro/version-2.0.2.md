Restrângeţi normele privitoare la vizibilitatea Hărţilor şi Detaliile repartizării pentru a se potrivi mai bine cu Territory Helper.

Dacă nu mai puteţi vedea toate hărțile pentru congregația voastră, atunci trebuie să:

  1. Modificaţi setarile Vestitorului congregaţiei dvs. în Territory Helper
  2. "Actualizaţi permisiunile" în ecranul Setări Teritoriu din cadrul aplicaţiei ServicePlanner

Pentru a înţelege mai bine permisiunile, consultați secţiunea Ajutor: 
https://serviceplanner.me/help/  
  
---  
  
Comentariile pozitive de pe App Store sunt de un real ajutor. Dacă vă place ServicePlanner şi aţi dori să lăsaţi un comentariu, ar fi foarte apreciat. Mulţumesc pentru sprijinul oferit :)

Site web: serviceplanner.me  
Ajutor: serviceplanner.me/help  
Detector de probleme: serviceplanner.me/issues  
Facebook: facebook.com/ServicePlannerApp
Îmi face plăcere să anunţ disponibilitatea aplicaţiei pentru utilizatorii de limbă olandeză, română şi japoneză.

Mulţumiri pentru munca grea a traducătorilor! Dacă doriţi să ajutaţi la traducerea aplicaţiei ServicePlanner într-o altă limbă, vă rog să mă contactaţi prin email support@serviceplanner.me.

Până data viitoare, bucuraţi-vă de ServicePlanner!

---

Comentariile pozitive de pe App Store sunt de un real ajutor. Dacă vă place ServicePlanner şi aţi dori să lăsaţi un comentariu, ar fi foarte apreciat. Mulţumesc pentru sprijinul oferit.

Site web: www.serviceplanner.me  
Ajutor: www.serviceplanner.me/help  
Detector de probleme: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp 
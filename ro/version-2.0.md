* Integrarea cu Territory Helper

* Plugin cu aspect nou pe ecranul astăzi
  - Alegeţi ce pluginuri să activaţi
  - Controlaţi ordinea pluginurilor
  - Plugin nou Activitatea 
  - Plugin nou Rezumatul orelor

* Suport pentru mai mulţi parteneri de predicare
  - Cu integrare în calendarul iOS

* Integrarea Hărţi Google îmbunătăţită

* Îmbunătăţiri pentru redarea materialelor video

* Posibilitatea de a adăuga 2 numere de telefon pentru fiecare persoană de contact

* Posibilitatea de a şterge plasările, materialele video şi vizitele din raport (dar a lăsa Contactul)

* Îmbunătăţiri multiple ale interfeţei de utilizare şi probleme rezolvate

Puteţi citi despre aceste noi caracteristici pe site-ul:
https://serviceplanner.me/blog/

---

Comentariile pozitive de pe App Store sunt de un real ajutor. Dacă vă place ServicePlanner şi aţi dori să lăsaţi un comentariu, ar fi foarte apreciat. Mulţumesc pentru sprijinul oferit :)

Site web: www.serviceplanner.me  
Ajutor: www.serviceplanner.me/help  
Detector de probleme: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp 
ServicePlanner to aplikacja twórców Equipd, która wspieraja służbę kaznodziejską i zarządzanie adresami. 

Aplikacja ServicePlaner została zaprojektowana specjalnie dla Świadków Jehowy, łączy rozbudowaną funnkcjonalność z prostym i atrakcyjny wyglądem. Jej zadaniem nie jest jedynie zliczanie czasu, ale dbanie o czas. Lepiej planuj swoją służbę już od dzisiaj!

* Synchronizacja przez Dropbox - automatyczne tworzenie kopii bezpieczeństwa oraz synchronizacja danych pomiędzy wieloma urządzeniami.

* Widok dnia dzisiejszego - szybki dostęp do nagrań wideo, publikacji i zaplanowanych na aktualny dzień odwiedzin ponownych. 

* Minutnik - zliczaj swój czas poświęcony na służbę kaznodziejską lub inne prace (LDPB, itp.) za pomocą licznika czasu. Uruchom/Wstrzymaj/Kontynuuj/Zatrzymaj minutnik z poziomu ikony aplikacji za pomocą 3D Touch (wybrane urządzenia).

* Zarządzanie adresami - zarządzaj swoimi odwiedzinami za pomocą listy adresów, którą można przeszukiwać. Przemyślane użycie kolorów pozwala na rozróżnienie rodzajów adresów oraz zorientowanie się, że odwiedziny są opóźnione lub zaplanowane. Przesuń palcem po adresie, aby szybko zaplanować następną wizytę lub dodać informacje o wizycie ponownej. Dotknij i przytrzymaj, aby uruchomić tryb wielowyboru, który pozwala na edycję większej ilości adresów jednocześnie.

* Biblioteka i historia odwiedzin - miej szybki dostęp do pełnej historii za pomocą listy wcześniejszych odwiedzin. Rodzaje odwiedzin są wskazywane kolorami. Określ datę oraz plan kolejnej wizyty. Biblioteka dla każdego adresu zawiera listę wszystkich publikacji, które kiedykolwiek zostały tam pozostawione.

* Filtrowanie adresów - rozbudowanie filtrowanie pozwala na zdefiniowanie sposobu grupowania i sortowania adresów. Wyszukiwanie adresów za pomocą tagów, języka, lokalizacji, daty następnych odwiedzin, a nawet pozostawionej publikacji lub bez pozostawienia publikacji.

* Widok mapy - pozwala na przegląd wizyt, wyszukiwanie adresów, umieszczanie własnych znaczników dla położenia samochodu, terenu, Sali Królestwa, domu lub innych obiektów. Dotknij i przytrzymaj, aby utworzyć adres bezpośrednio w miejscu na mapie.

* Biblioteka - miejsce centralnego zarządzania publikacjami i filmami, które zamierzasz używać w służbie. Dodaj okładkę, aby móc wzrokowo rozpoznawać publikacje i filmy. Możliwe jest podanie linków do filmów, pozwala to na ich pobranie do użycia offline lub na bezpośrednie odtwarzanie, które nie obciąża pamięci urządzenia. Dodaj rzeczy do widoku dnia dzisiejszego.

* Aktywność w miesiącu - planuj głoszenie widząc cel godzinowy oraz już spędzoną w służbie ilość godzin. Zobacz podgląd na żywo aktualnego stanu sprawozdania. Zaplanowany czas służby pokazuje, czy udaje Ci się go dotychczas realizować w danym miesiącu oraz czy trzymasz się planu.

* Rok służbowy - zobacz zestawienie godzin służby w podziale na miesiące oraz podsumowanie działalności w roku służbowym. Zaplanowana ilośc godzin pokazuje, czy zdążasz do celu godzinowego, który chcesz osiągnąć w danym roku. 

* Planowanie - zdefiniuj swój roczny, miesięczny i dzienny cel godzinowy. Będą one użyte do śledzenia postępu w miesiącu i roku. Zastosuj swój dzienny plan do miesiąca i zobacz zaplanowane godziny - łatwo zobaczysz czy pozwoli to na osiągnięcie celu, czy trzeba będzie zmodyfikować plan służby.

* Raportowanie - pełna obsługa godzin zaległych oraz minut do przeniesienia na kolejny miesiąc. Twoje miesięczne sprawozdanie może być wysłane za pomocą e-maila, pliku tekstowego, aplikacji Telegram lub WhatsApp. Wpisz dane swojego nadzorcy i opcji sprawozdania w Ustawieniach. 

* „Inne godziny” - zapisuj czas poświęcony na inne zajęcia niż służba, np. współpracę z LDPB albo współpracę z Betel. Można je planować pomiędzy czasem służby. Konfiguruj kategorie (np. „LDPB”) i opcje raportowania w Ustawieniach.

---

To jedynie krótki przegląd aktualnych funkcji aplikacji ServicePlanner w wersji 1.0. Jest to dopiero początek - w planach jest jeszcze wiele funkcji.

Zachęcam do zaglądania na stronę internetową, która będzie opisywała funkcje szczegółowo i zawierała przykładowe wyglądy ekranów: www.serviceplanner.me

Powodzenia!

Wymagania: iOS 10 lub nowszy
Zoptymalizowane dla iPhone'a oraz iPada.
Interfejs użytkownika jest aktualnie przetłumaczony na język angielski i niemiecki.
Wsparcie techniczne jest świadczone przede wszystkim w języku angielskim. 
Mapy wykorzystują pamięć podręczną offline, jednak zasadniczo wymagają urządzenia z dostępem do Internetu.
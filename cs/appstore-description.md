ServicePlanner is a Field Ministry and Contact Management app designed for Jehovah's Witnesses.

* Dropbox Sync - automatically backup your data and sync across multiple devices.

* Today Screen - quick access to the Timer, Videos, Publications, Website Browser, Activity Planner and your scheduled calls for the day. 

* Timer - record your time spent in the ministry or on "other" work (LDC etc). Tap the timer bar on the Today screen or force touch the app icon.

* Contact Management - centrally manage your Calls with a searchable Contact list. Colours are carefully used to indicate the Contact Type and if your visits are overdue or on schedule. Slide on a Contact to quickly reschedule their next visit, or add a Return Visit. Tap+hold to enter multi-select mode and edit multiple contacts at once.

* Contact Visit History & Library - quickly access your call history with a list of previous visits. Colour again is used to visually indicate the Visit Type. Define your Next Visit Date and Plans. A central Library for each contact shows all of the publications you have ever placed with them.

* Contact Filtering - powerful filtering allows you to define how you Group and Sort your calls. Search for Contacts using Tags, Language, Locality, Next Visit Date, and even by Publications Placed or Not Placed.

* Map View - get an overview of your Calls, search for addresses, place custom markers for the location of your Car, Territory, Kingdom Hall, your Home and more. You can tap+hold to create a new Contact straight from a map location.

* Library - centrally manage the Publications and Videos that you will place with your calls. Add Cover Art to visually identify the Publications and Videos. Video locations can be entered to allow you to download for offline use, or to live stream and free up storage space on your device. Add items to your Today screen. Note that the Library is empty by default, users will need to populate it themselves.

* Websites - use the inbuilt browser to show web content. Bookmark your favourite websites. Supports Reader View with Themes. Reader View supports Chinese Pinyin, Zhuyin, Yale, Sidney Lau above Chinese characters when reading content from the official websites.

* Monthly Activity - plan your daily activity with Planned Hours vs Actual Hours. Projected Hours indicate if you are on track with your time.

* Service Year - see at a glance your hours per month and a summary of your activity for the Service Year. Projected Hours indicate if you are on target to reach your annual goal. 

* Scheduling - define your Annual, Monthly and Daily Hour Goals. These will then be used to track your progress for the Month and Year. Apply your Daily Schedule to your Month and view the Projected Hours - so easy to see if you will get your time in or if you need to alter your schedule.

* Witnessing Partners - arrange your partners each day. Contact your witnessing partner by SMS, Phone, or messaging apps. Mark the arrangement as confirmed or not. Icons on the calendar indicate days with arranged partners.

* Reporting - full support for multiple Hour Credits and Carry-Over Minutes. Your Monthly Report can be submitted using Email, TXT, Telegram or WhatsApp. Setup your overseers details and your report options in Settings. 

* "Other Hours" - log your non-ministry hours such as LDC or Branch Consultation work. These can be scheduled and planned alongside your Ministry Hours. Configure the category name (eg "LDC")  and Reporting options in Settings.

---

This is just the beginning - there are so many more features planned.

I strongly encourage you to check out the website which further details the features and shows example screenshots: www.serviceplanner.me

Enjoy!

Requirements: iOS 10+
Optimised for the iPhone and iPad.
Some mapping features require a connected device.
All support and documentation is provided in English.
The UI is translated into German, Italian, Spanish, Portuguese, Dutch, Romanian, Japanese.
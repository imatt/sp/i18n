APP STORE DESCRIPTION  
This is used on the main app page on the App Store.  
It must be less than 4,000 characters in length.  

Only translate below the following line...  

---

ServicePlanner är en Tjänste och Kontakthanterings-app som är utformad specifikt för Jehovas Vittnen.

• Dropbox Synkning - säkerhetskopiera dina data automatiskt och synka mellan flera enheter.

• Integration med Territory Helper.

• Aktuellt Idag - få snabb tillgång till Timer/Tidtagare, Videor, Publikationer, Webbläsare, Aktivitetsplanerare, och dina schemalagda besök för dagen.

• Timer/Tidtagare - ta tiden medan du är i tjänsten eller när du utför annan typ av tjänst (T.ex LDC). Tryck bara på timer-fältet under Aktuellt Idag eller använd 3D touch på appikonen.

• Kontakthantering - hantera dina besök centralt och enkelt med en sökbar Kontaktlista. Färger används noggrant för att indikera typ av besök och om dina besök är försenade eller uppkommande. Svep med fingret på en Kontakt för att snabbt boka om deras nästa besök, eller för att lägga till ett Återbesök. Tryck och håll in med fingret för att aktivera flerväljaren och markera och redigera flera kontakter på samma gång.

• Historik över kontaktbesök & Bibliotek - få snabb tillgång till din besökshistorik med en lista över föregående besök. Ange ditt nästa planerade besök och vad du planerar ta upp eller visa vid besöket. Ett centralt bibliotek för varje kontakt visar alla publikationer du har placerat hos dem.

• Kontaktfiltrering - sortera Grupper och dina besök. Sök efter Kontakter genom att använda Taggar, Språk, Lokalitet, Nästa Besöksdatum, och till och med efter vilka publikationer du placerat eller inte placerat.

• Kartläge - få en översikt över dina besök, sök efter adresser, lägg till egna markeringar för var din bil är parkerad, Distrikt, Rikets Sal, ditt eget hem med flera. Du kan trycka och hålla in fingret på kartan för att skapa en ny kontakt.

• Bibliotek - hantera dina Publikationer och videor på en central plats i appen. Lägg till valfria framsidor på videor och publikationer för att enklare kunna visualisera dem. Videor kan laddas ner för användning offline, eller streamas för att fria upp lagringsutrymme. Lägg till objekt under Aktuellt Idag. Tänk på att biblioteket är tomt när appen installerats, och att användare själva måste lägga till publikationer och videor.

• Webbsidor - använd den inbyggda webbläsaren för att visa webbinnehåll. Bokmärk dina favorithemsidor. Stödjer Läs-läge med teman. Läs-läge stödjer kinesisk Pinyin, Zhuyin, Yale, Sidney Lau över Kinesiska tecken när du läser innehåll från de officiella hemsidorna.

• Månatlig Aktivitet - planera din dagliga aktivitet via Planerade timmar mot Faktiska timmmar. Beräknade timmar indikerar om du förväntas att klara dina timmar.

• Tjänsteår - med ett snabbt ögonkast ser du dina timmar per månad och får en översikt över din aktivitet för tjänsteåret.

• Schemaläggning - ange dina årliga, månatliga och dagliga tim-mål. Dessa används sen för att följa dina framsteg för månaden och året. Tillämpa ditt dagliga schema på din månad och se beräknat antal timmar - så enkelt det blir att se om du kommer klara din tid eller om du måste justera ditt schema.

• Samarbete i Tjänsten - planera och schemalägg samarbete för tjänsten varje dag. Kontakta din samarbetspartner via SMS, Telefon, eller via andra meddelande-appar. Markera samarbetet som bestämt eller preliminärt. Ikoner i kalendern visar dagar du har bestämt samarbete. Valfri integration med iOS-kalendrar.

• Rapportera - stöd för att föra över timmar och minuter till nästa månad. Din månatliga rapport kan skickas via Epost, SMS, WhatsApp, LINE eller Hourglass. Ange din tillsyningsmans uppgifter och dina rapportinställningar under Inställningar.

• "Andra timmar" - lägg till dina special-timmar till exempel LDC eller Konsultationsjobb åt Avdelningskontoret. Dessa kan schemaläggas och planeras jämte dina tjänstetimmar. Konfigurera kategorinamnet (T.ex "LDC") och rapportinställningar under Inställningar.

---

Krav: iOS 10+
Vissa karttjänster kräver en ansluten enhet.  
Support och dokumentation finns enbart på engelska.
www.serviceplanner.me
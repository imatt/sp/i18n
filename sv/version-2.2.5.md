• Appen stöder nu svenska - tack Johannes! 

• Lagt till begränsat stöd för utdelning av kartor/distrikt som används under speciella kampanjer via Territory Helper. Vissa användare får enbart se kartor som är tilldelade dem. Eftersom ServicePlanner inte hade stöd för kampanjer, så kunde inte dessa användare se kartor som tilldelats dem som en del i en kampanj för inbjudan till Minneshögtiden. Dessa användare kan nu se de kartor/distrikt som tilldelats dem och kan lämna in distriktet när det är bearbetat/klart.

• Mindre felrättningar.

---

Positiva recensioner i App store är till stor hjälp. Om du gillar ServicePlanner och du känner dig motiverad att lämna en recension, så uppskattas det mycket! Tack för ditt stöd :)

Webbsida: serviceplanner.me 
Hjälp/Dokumentation: serviceplanner.me/help 
Problem/Buggar: serviceplanner.me/issues 
Facebook: facebook.com/ServicePlannerApp
Twitter: https://twitter.com/ServicePlanner_
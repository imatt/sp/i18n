• New GDPR Compliance mode for users in the UK and EU.

• Territory Helper integration updated to match recent GDPR related changes.
  
• The app now supports Korean and Danish users!

• Bug fixes and improvements.

Please see the website for full details on this update: www.serviceplanner.me/blog

Positive reviews on the App Store are incredibly helpful. If you are enjoying ServicePlanner, and feel inspired to leave a review, it would be greatly appreciated. Thanks for your support.

---

Website: www.serviceplanner.me  
Help Docs: www.serviceplanner.me/help  
Latest News: www.serviceplanner.me/blog
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
Twitter: www.twitter.com/ServicePlanner_
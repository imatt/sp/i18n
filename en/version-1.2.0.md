Full details including screenshots and videos of this update can be found on the blog: https://serviceplanner.me/blog

This is quite a large update...


Report Detail Screens
======================  

On the Activity screen, each line of the Report Summary can now be clicked on to view more detail. 

For example, click on Placements to get a full breakdown of Placements per day for the month. You can click on each line item to jump to the Contact or the Visit details.

This provides a lot more visibility around your activity for the month and how the report metrics have been calculated.   


Drag & Drop Contact Markers on the Map
=======================================

You can now tap on a Contact marker, and it will enter "move mode". You will see crosshairs appear. You can now drag that marker to a new location.

Simply tap elsewhere on the map to save the new location for that Contact.

There were many other enhancements to the mapping, especially useful for countries where addresses are not so accurate or useful. See the Blog for more details.


Custom Saved Filters
=====================

You can now save your favourite Filter settings to reuse them again. In the Filter window, scroll to the bottom of the "Current" tab and hit the "Save Current Filter Settings" button.

You can now edit the default Saved Filters. You can edit, rename and delete Filters. You can also re-order the Filters with the Top filter becoming the "Default" filter.

On the main Contact screen tap+hold on the Filter button to quickly apply your Default Filter settings. 


Videos
=======

Videos shown to a Contact can now be viewed inside the Contacts Library tab. The Contact Library gives you a quick overview of all the Publications that you have placed with that Contact, or all the Videos they have been shown.

You can now Filter on Videos. This is extremely powerful. For example use the Filters to list all Contacts that have not yet seen a specific Video.


Activity Notes
===============

You can now enter a Note for each day in the Activity Daily Report screen. Displayed just under the Hours fields this provides an opportunity to explain what type of LDC or Consulting work you were doing, or enter any other details you would like to remember for that day.

A summary of these Notes for the month can also be viewed from the Hours detail screen.

The Notes can also be optionally included in your Report. You will need to enable this in Settings.  


List of Visits
===============

When viewing the list of Visits made to a Contact, the list will now itemise each Publication or Video from that Visit. 

eg. Before it might show "1 x Awake!" but now it will show "Awake! No. 3 (June) - Is the Bible Really From God?".   


Other Improvements
===================

There have been many other tweaks and improvements to the app. 

For example in Settings > Reporting you can now choose to hide line items with a value of 0 in your final report. For example, if you did not have any Bible Studies instead of it showing "Bible Studies: 0" in your final report, it will hide that line item.

For Publications in the "Other" category, you can choose if the Publication is counted as a Placement or not. This could be helpful for an item that you want to record as giving to the Contact, but should not be counted as a placement.  

Also the entire app has now been translated into Spanish and Italian! Thanks so much to our hardworking translators. Work on other languages is underway. If you would like to help translate ServicePlanner into your language, please log your interest on the Issue Tracker (see below).  

---

That's it for this update. As mentioned, don't forget to check the blog for further details. 

I hope this helps you to be more effective in your ministry and get the most out of using ServicePlanner.

Positive reviews on the App Store are incredibly helpful. If you are enjoying ServicePlanner, and feel inspired to leave a review, it would be greatly appreciated. Thanks for your support.

---

Website: www.serviceplanner.me  
Help Docs: www.serviceplanner.me/help  
Latest News: www.serviceplanner.me/blog   
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp  
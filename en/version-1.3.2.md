[v1.3.2]  Fixed issue with Activity > Bible Studies screen in some Time Zones.

[v1.3.1]  Various bug fixes and improvements.

* Improved the refreshing of the Contacts Library when placements are updated.

* Fixed a bug with the Field Service Report email subject showing the incorrect month in some Time Zones.

[v1.3.0]  Full details of this update can be found on the website: https://serviceplanner.me/blog

*** Witnessing Partners ***  
On the Activity Calendar, you can now select a Partner for each day. Tap their name to call or message them. Use to toggle to indicate if it is a "Confirmed" arrangement. You will receive a reminder notification, customisable in Settings > Notifications.

*** Report Reminder ***  
You will now receive a reminder to submit your field service report. You can customise this reminder reminder in Settings > Notifications.   

*** Visits Reminder ***  
You will now receive a reminder about upcoming Visits you have scheduled. You can customise this reminder reminder in Settings > Notifications.   

*** App Badge Indicator ***  
The App Icon will now show a red "1" to indicate that the Timer is running. This can be disabled in Settings > Timer.   

*** Import Device Contacts ***  
If you tap+hold on the "+" button on the Calls tab, there is the option to "Import from Device Contacts". This is a basic importer that allows you to pull in existing Contact details from your Address Book. See the website for full details on how this works.

*** Messaging a Contact ***
If a Contact has a Phone number, the "message" button will now allow messages to be sent via Telegram, WhatsApp, Viber and other apps - depending on what apps you have installed on your device.

*** Background Sync ***  
When the app goes into the background it will now perform a single "push" of any changes made on your local device to Dropbox. This will trigger if you press the Home button while using ServicePlanner, or switch apps, or turn off your device. This ensures that you do not lose any changes and that data is in sync between devices.  

*** Improved Time Zone Support ***  
Some users in places like Paraguay were experiencing various issues. These have been resolved.

*** Portuguese ***  
ServicePlanner is now translated for Portuguese (PT) users! If you would like to help translate the app into your language please log your interest on the Issue Tracker (see below).

---

I hope this update finds everybody well and that you are enjoying ServicePlanner.

Positive reviews on the App Store are incredibly helpful. If you are enjoying ServicePlanner, and feel inspired to leave a review, it would be greatly appreciated. Thanks for your support.

Website: www.serviceplanner.me  
Help Docs: www.serviceplanner.me/help  
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
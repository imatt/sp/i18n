* Integration with Territory Helper (www.territoryhelper.com)  

* New Plugin Layout on Today Screen  
  - Choose which Plugins to enable  
  - Control the order of Plugins  
  - New Activity Plugin  
  - New Hours Summary Plugin  
  
* Support for Multiple Witnessing Partners  
  - With iOS Calendar integration
  
* Improved Google Maps Integration  
  
* Improvements to Playing Videos  
  
* Ability to add 2x Phone Numbers per Contact  
  
* Ability to Delete Placements, Videos, Visits from your Report (but leave against the Contact)  
  
* Added ability to send report via LINE  
  
* Multiple UI improvements and issues resolved  
  
* 2.0.1 resolves some minor issues from version 2.0.  
  
You are encouraged to read about these new features on the website: https://serviceplanner.me/blog/  
  
---  
  
Positive reviews on the App Store are incredibly helpful. If you are enjoying ServicePlanner and feel inspired to leave a review, it would be greatly appreciated! Thanks for your support :)

Website: serviceplanner.me  
Help Docs: serviceplanner.me/help  
Issue Tracker: serviceplanner.me/issues  
Facebook: facebook.com/ServicePlannerApp
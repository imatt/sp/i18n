* Tightened the rules around visibility of Maps and Assignment details to better match Territory Helper. 

* Numerous improvements and bug fixes.

Note: If you can no longer see all maps for your Congregation then you need to check that your brothers have enabled the "Publishers can view all territories" setting inside Territory Helper. If they change this, then you will need to open the Territory Settings screen in ServicePlanner and "Refresh Permissions".

See the website Blog and Help Docs for a more detailed explanation of these changes.
  
---  
  
Positive reviews on the App Store are incredibly helpful. If you are enjoying ServicePlanner and feel inspired to leave a review, it would be greatly appreciated! Thanks for your support :)

Website: serviceplanner.me  
Help Docs: serviceplanner.me/help  
Issue Tracker: serviceplanner.me/issues  
Facebook: facebook.com/ServicePlannerApp
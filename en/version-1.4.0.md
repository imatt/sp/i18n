New in-app website browser: 

- Accessed from the top toolbar on both the Library and Today screens.
- Works very similar to Safari.
- Type in a search phrase to search using Google.
- Type in a URL to directly access a website. 
- Bookmark your favourite websites.
- Bookmarks can be viewed in Grid or List View.
- Similar to most browsers, website icons are automatically detected.
- In Grid View tap+hold on a Bookmark icon to edit.
- In List View you can reorder the Bookmarks.
- Supported Videos are detected and can be saved to the app.
- Reader View support, similar to Safari and Firefox.
- Review View has themes including Night Mode and custom font support.
- Reader View content can be emailed, similar to Safari. 
- Reader View supports Chinese Pinyin, Zhuyin, Yale, Sidney Lau above Chinese characters when reading content from the official websites. Great for Chinese language learners.

From the Today screen top/right you can now quickly access the Activity screen for the current date.

On the Daily Activity screen there are now +/- buttons to increase/decrease manual Placements and Videos Shown. Top+hold on the "+" to manually enter an amount. Tap+hold on the "-" to reset to 0.

Several bug fixes and other improvements are also included in this update. Please read more about this update on the website blog.

---

I hope this update finds everybody well and that you are enjoying ServicePlanner.

Positive reviews on the App Store are incredibly helpful. If you are enjoying ServicePlanner, and feel inspired to leave a review, it would be greatly appreciated. Thanks for your support.

Website: www.serviceplanner.me  
Help Docs: www.serviceplanner.me/help  
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp 
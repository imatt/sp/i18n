I am pleased to announce availability for Dutch, Romanian and Japanese users. Thanks to the hard working translators! If you would like to help translate ServicePlanner into your language please email support@serviceplanner.me.

Until next time, enjoy ServicePlanner!

---

Positive reviews on the App Store are incredibly helpful. If you are enjoying ServicePlanner, and feel inspired to leave a review, it would be greatly appreciated. Thanks for your support.

Website: serviceplanner.me  
Help Docs: serviceplanner.me/help  
Issue Tracker: serviceplanner.me/issues  
Facebook: facebook.com/ServicePlannerApp 
Full details of this update can be found on the website https://serviceplanner.me:

*** Timer Support ***  
Added by popular demand. Just tap the new Timer bar on the Today screen to Start / Pause / Resume the Timer. While in Pause mode you can Stop the Timer and choose to apply the recorded time against Ministry Hours or "Other" Hours (LDC etc). The Timer will resume if you close the app or restart your device. In Settings you can change the colour of the running timer bar to match your preferences. There is also a Log of timer entries kept in the settings.

*** 3D Touch Support ***  
If your device supports Force Touch you can use the App Icon to +Add Contact or Start / Pause / Resume / Stop the Timer.

*** Custom Date Option ***  
If you swipe on a Contact in the list, or tap+hold to select multiple Contacts, you can change the "Next Visit Date" for one or more contacts. Before you could choose Today, Tomorrow, +1 Week, +2 Weeks etc. Now you can also choose "Custom" and manually select the specific date when you would like to visit. 

*** Split View Support ***  
If your iPad supports Split View and Slide Over then you will enjoy this update.

*** German Localisation ***  
While the app allows users to enter Publications and Videos in any language, the app UI (buttons and labels etc) were English only. Thanks to the hard work of one enthusiastic user of ServicePlanner the entire app is now translated for German users! If you would like to help translate the app into your language please log your interest on the Issue Tracker (see below).

That's it for this update. I guess that is actually quite a lot considering the app was only released just over a week ago! Thanks for your positive feedback so far. This is just the beginning - there is a lot more planned for ServicePlanner!

---

Positive reviews on the App Store are incredibly helpful. If you are enjoying ServicePlanner, and feel inspired to leave a review, it would be greatly appreciated. Thanks for your support.

Website: www.serviceplanner.me  
Help Docs: www.serviceplanner.me/help  
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
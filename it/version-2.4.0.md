• Nuova conformità al regolamento sulla protezione dei dati (GDPR) per gli utenti UK e UE.

• Aggiornamento dell'integrazione di Territory Helper per allinearsi ai recenti cambiamenti GDPR.
  
• L'app adesso supporta gli utenti Coreani!

• Correzioni di bug e miglioramenti.

Per favore consulta il sito web per dettagli completi su questo aggiornamento: www.serviceplanner.me/blog

Le recenzioni positive sull'App Store sono incredibilmente utili. Se ti piace ServicePlanner, la tua recenzione sarà molto apprezzata. Grazie per il tuo contributo.

---

Sito web: www.serviceplanner.me  
Documentazione di aiuto: www.serviceplanner.me/help  
Ultime novità: www.serviceplanner.me/blog
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
Twitter: www.twitter.com/ServicePlanner_
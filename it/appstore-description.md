ServicePlanner è un'applicazione per il ministero di campo e la gestione delle visite dello sviluppatore di Equipd.

Progettato per i Testimoni di Geova, ServicePlanner combina potenti funzionalità con un'interfaccia intuitiva e attraente. L'obiettivo non è solo di cronometrare il tempo, ma anche di tenere conto del tuo tempo. Programma e pianifica il tuo ministero da oggi!

* Sincronizzazione Dropbox - eseguire automaticamente il backup dei dati e la sincronizzazione su più dispositivi.

* Schermata Oggi - accedere rapidamente ai video, alle pubblicazioni e alle visite pianificate per oggi.

* Cronometro - registrare il tempo trascorso nel ministero o in un "altra" attività (LDC, ecc.) utilizzando il cronometro. Avvio / Pausa / Riprendi / Arresta il cronometro dall'icona dell'applicazione con Force Touch.

* Gestione contatti - gestire centralmente le visite con un elenco di contatti ricercabili. I colori vengono accuratamente utilizzati per indicare il tipo di contatto e se le visite sono in ritardo o secondo programma. Scorrere su un contatto per pianificare rapidamente la prossima visita o aggiungere una visita ulteriore. Tocca e tieni premuto il tasto + per accedere alla modalità multi-selezione e modificare più contatti contemporaneamente.

* Storico e pubblicazioni della visita - accedi rapidamente alla cronologia dei contatti con un elenco delle visite precedenti. Il colore viene nuovamente utilizzato per indicare visivamente il tipo di visita. Definisci e pianifica la data della tua prossima visita. Una biblioteca centrale per ogni contatto mostra tutte le pubblicazioni che hai distribuito loro.

* Filtro contatti - un potente filtraggio consente di definire il modo in cui definire e ordinare le visite. Cercare i contatti usando i tag, la lingua, la località, la data della prossima visita e persino le pubblicazioni distribuite o non distribuite.

* Vista mappa - visualizzare una panoramica delle visite, ricercare degli indirizzi, inserire marcatori personalizzati per la posizione della tua auto, del territorio, della Sala del Regno, della tua casa e altro ancora. È possibile toccare e tenere premuto il tasto + per creare un nuovo contatto direttamente da una posizione geografica.

* Pubblicazioni - gestire centralmente le pubblicazioni e i video che verranno distribuitialle tue visite. Aggiungere la copertina per identificare visivamente le pubblicazioni e i video. È possibile immettere i link ai video per consentire il download per uso in modalità offline oppure per lo streaming dal vico e liberare lo spazio di archiviazione sul dispositivo. Aggiungere elementi alla schermata Oggi.

* Attività mensile - pianificare l'attività quotidiana con ore programmate rispetto a ore effettive. Visualizzare un'istantanea del progresso del tuo rapporto. Le ore proiettate indicano se sei in linea con le ore attuali durante il mese e se rimani in programma.

* Anno di servizio - dare un'occhiata alle ore del mese e un riepilogo della tua attività per l'anno di servizio. Le ore proiettate indicano se sei in linea per raggiungere l'obiettivo di ore per l'anno.

* Pianificazione - definire i tuoi obiettivi annuali, mensili e giornalieri. Questi saranno poi utilizzati per monitorare il progresso del mese e dell'anno. Applicare la tua agenda giornaliera al tuo mese e visualizza le ore proiettate - è così facile vedere se raggiungerai il tuo obiettivo o se devi modificare il tuo programma.

* Rapporto - pieno supporto per crediti di ore e riporto dei minuti. Il tuo rapporto mensile può essere inviato tramite E-mail, TXT, Telegram o WhatsApp. Imposta i dettagli del sorvegliante e le opzioni del rapporto nelle impostazioni.

* "Altre ore" - registrare le tue ore di non-ministero come il lavoro per LDC o come Collaboratore della Filiale. Queste possono essere programmate e pianificate accanto alle ore del ministero. Configurare il nome della categoria (ad esempio "LDC") e le opzioni di rapporto nelle impostazioni.

---

Questa è solo una rapida panoramica delle funzionalità attuali di ServicePlanner nella versione 1.0. È solo l'inizio - ci sono tante altre funzioni pianificate.

Vi incoraggio fortemente a controllare il sito web che descrive dettagliatamente le funzionalità e mostra immagini di esempio: www.serviceplanner.me

Buon divertimento!

Requisiti: iOS 10+
Ottimizzato per iPhone e iPad.
L'interfaccia è attualmente solo in inglese, italiano e tedesco.
Il supporto è fornito in inglese, ma proverò anche in altre lingue.
Le mappe hanno una cache offline ma complessivamente è richiesto un dispositivo connesso.
Correzioni di bug e miglioramenti.

Consulta il sito web per informazioni dettagliate su questo aggiornamento:
https://serviceplanner.me/blog

Le recensioni positive sull'App Store sono incredibilmente utili. Se ti piace l'app ServicePlanner e ti senti ispirato, sarebbe molto apprezzato se potessi lasciare una recensione. Grazie per il tuo contributo.

---

Sito Web: www.serviceplanner.me  
Documentazione di aiuto: www.serviceplanner.me/help   
Ultime novità: www.serviceplanner.me/blog 
Issue Tracker: www.serviceplanner.me/issues 
Facebook: www.facebook.com/ServicePlannerApp 
I dettagli completi, inclusi screenshot e video, di questo aggiornamento si trovano sul blog: https://serviceplanner.me/blog

Questo è un grande aggiornamento...


Schermata Dettaglio Rapporto
=============================

Nella schermata Attività, ogni riga del riepilogo del rapporto può ora essere cliccata per visualizzare ulteriori dettagli.

Ad esempio, fai clic su Pubblicazioni per ottenere una ricapitolazione completa delle pubblicazioni per ogni giorno del mese. Puoi fare clic su ogni elemento per passare al Contatto o ai dettagli della Visita.

Questo fornisce una maggiore visibilità riguardo alla tua attività del mese e come sono stati calcolati i valori del rapporto.


Trascina & Rilascia i marcatori dei Contatti nella Mappa
=========================================================

Ora puoi cliccare un marcatore del Contatto e entrerai in "modalità di spostamento". Appariranno i mirini e potrai trascinare quel marcatore in una nuova posizione.

Basta toccare altrove sulla mappa per salvare la nuova posizione del Contatto.

Ci sono stati molti altri miglioramenti alla mappatura, particolarmente utile per i paesi in cui gli indirizzi non sono così accurati o di facile utilizzo. Vedi il Blog per ulteriori dettagli.


Filtri personalizzati
======================

Ora puoi salvare le impostazioni preferite del Filtro per riutilizzarle di nuovo. Nella finestra Filtro, scorrere fino alla parte inferiore della scheda "Attuale" e premere il pulsante "Salva impostazioni di filtro attuale".

Ora è possibile modificare i filtri predefiniti. È possibile modificare, rinominare ed eliminare i filtri. È inoltre possibile ordinare nuovamente i filtri impostando il filtro superiore come filtro "predefinito".

Nella schermata principale dei Contatti clicca e tieni premuto sul tasto Filtro per applicare rapidamente le impostazioni del filtro predefinito.


Video
======

I video mostrati ad un contatto possono essere visualizzati nella scheda Pubblicazioni dei Contatti. Questa sezione fornisce una rapida panoramica di tutte le pubblicazioni che hai distribuito a quel contatto o di tutti i video che sono stati mostrati.

Ora puoi filtrare i video. Questo è estremamente performante. Ad esempio puoi utilizzare i Filtri per elencare tutti i contatti che non hanno ancora visto uno specifico video.


Note Attività
==============

È ora possibile inserire una nota per ogni giorno nella schermata Rapporto dell'attività giornaliera. Visualizzato appena sotto i campi Ore, le note permettono di spiegare il tipo di lavoro svolto per LDC o per la Consulenza oppure di immettere qualsiasi altro particolare che si desidera ricordare per quel giorno.

Un riepilogo di queste Note del mese può essere visualizzato anche dalla schermata Dettagli ore.

Le Note possono anche essere incluse nel tuo Rapporto. Dovrai abilitare questa possibilità nelle Impostazioni.


Elenco delle Visite
====================

Quando si visualizza l'elenco delle visite effettuate in riferimento a un contatto, l'elenco mostra ogni pubblicazione o video per quella visita.

Per esempio, prima veniva mostrato "1 x Svegliatevi!" mentre adesso verrà mostrato "Svegliatevi! No. 3 (giugno) - La Bibbia è davvero la Parola di Dio?".


Altri miglioramenti
====================

Ci sono state molte altre modifiche e miglioramenti.

Ad esempio, in Impostazioni > Rapporto puoi scegliere di nascondere nel rapporto finale elementi con un valore uguale a 0. Ad esempio, se non hai fatto studi biblici invece di mostrare "Studi biblici: 0" nel tuo rapporto finale verrà nascosta quella riga.

Per le pubblicazioni nella categoria "Altro" è possibile scegliere se la pubblicazione è contata come Pubblicazione oppure no. Ciò potrebbe essere utile per un elemento che si desidera registrare come dato al Contatto, ma che non dovrebbe essere conteggiato come pubblicazione distribuita.

L'intera applicazione è stata ora tradotta in spagnolo e italiano! Ringraziamo molto i nostri traduttori per l'inteso lavoro. La traduzione in altre lingue è in corso. Se vuoi aiutare a tradurre ServicePlanner nella tua lingua, ti preghiamo di annunciare il tuo interesse su Issue Tracker (vedi sotto).

---

Questo è tutto per questo aggiornamento. Spero che ti aiuti ad essere più efficace nel tuo ministero e sfruttare al meglio ServicePlanner.

Le recensioni positive sull'App Store sono incredibilmente utili. Se ti piace l'app ServicePlanner e ti senti ispirato, sarebbe molto apprezzato se potessi lasciare una recensione. Grazie per il tuo contributo.

---

Sito Web: www.serviceplanner.me  
Documentazione di aiuto: www.serviceplanner.me/help  
Ultime novità: www.serviceplanner.me/blog   
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp  
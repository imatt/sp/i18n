Questo è un veloce aggiornamento con solo una nuova funzione...

Nella schermata principale Visite c'è ora un pulsante "Condividi contatto".

Questa funzione è al momento molto semplice. Invia tramite e-mail i dettagli del contatto con la cronologia completa delle visite. Non vi è l'opzione per un altro utente di ServicePlanner per importare i dettagli. Questa possibilità sarà prevista in futuro. Al momento, l'obiettivo è di fornire un metodo di base per condividere i dettagli dei contatti con un altro proclamatore. Per far funzionare questo pulsante è necessario che il dispositivo sia configurato per inviare e-mail utilizzando l'app Mail predefinita.

Alla prossima, divertiti con ServicePlanner!

---

Le recenzioni positive sull'App Store sono incredibilmente utili. Se ti piace ServicePlanner, la tua recenzione sarà molto apprezzata. Grazie per il tuo contributo.

Sito Web: www.serviceplanner.me  
Documentazione di aiuto: www.serviceplanner.me/help  
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp 
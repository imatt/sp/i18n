Nuova opzione Condividi Contatto
================================

* La condivisione tramite e-mail include ora un file in allegato che un altro utente di ServicePlanner può utilizzare per importare direttamente il contatto.

* È possibile condividere solo il file (senza inviare l'e-mail) e trasmetterlo tramite AirDrop o utilizzando app di messaggistica come Telegram, WhatsApp, Viber ecc.


Nuovo Backup / Restore manuale
==============================

In alternativa a Dropbox Sync, ora puoi utilizzare la nuova opzione Backup manuale. Invia il backup tramite AirDrop a un altro dispositivo o memorizzalo utilizzando un servizio cloud o un'app di condivisione file. Puoi anche inviare tramite e-mail il backup tramite l'app Mail predefinita, che utilizzerà la soluzione Mail Drop gratuita per archiviare il file per 30 giorni. Il punto è: puoi scegliere.

Per ripristinare il backup, è sufficiente individuare il file ZIP e scegliere "Apri in ..." ServicePlanner.

La funzione Backup manuale include un servizio Promemoria facoltativo: scegliere di ricevere un promemoria ogni 7, 14, 30 o 60 giorni.

Puoi leggere queste nuove funzionalità sul Blog di ServicePlanner che le spiega in modo più dettagliato.

---

Le recenzioni positive sull'App Store sono incredibilmente utili. Se ti piace ServicePlanner, la tua recenzione sarà molto apprezzata. Grazie per il tuo contributo.

Sito Web: www.serviceplanner.me
Documentazione di aiuto: serviceplanner.me/help
Issue Tracker: serviceplanner.me/issues
Facebook: facebook.com/ServicePlannerApp
Fejlrettelser og forbedringer.

Besøg hjemmesiden for at få et fuldt overblik over hvad denne opdatering indeholder: https://serviceplanner.me/blog

Konstruktive anmeldelser i App Store er utrolig nyttige. Hvis du har gavn af ServicePlanner, og har lyst til at efterlade en anmeldelde, vil det være meget værdsat. Mange tak for din støtte.

---

Hjemmeside: www.serviceplanner.me  
Hjælp: www.serviceplanner.me/help  
Sidste nyt: www.serviceplanner.me/blog
Fejlhåndtering: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
Twitter: www.twitter.com/ServicePlanner_
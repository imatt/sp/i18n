• Nieuw actiemenu in het scherm Activiteit > Jaar om de gegevens van het jaarverslag handmatig te kunnen verversen.

• De app ondersteunt nu Noors - bedankt John!

• Bug fixes and improvements.

Zie de website voor meer informatie over deze update: www.serviceplanner.me/blog

Positieve beoordelingen in de App Store zijn ongelooflijk handig. Als u van ServicePlanner geniet en u bent geïnspireerd om een reactie achter te laten, zou dit zeer op prijs worden gesteld! Bedankt voor je steun :)

---

Website: www.serviceplanner.me  
Help Docs: www.serviceplanner.me/help  
Latest News: www.serviceplanner.me/blog
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
Twitter: www.twitter.com/ServicePlanner_
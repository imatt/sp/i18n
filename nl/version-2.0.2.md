De regels rond de zichtbaarheid van kaarten en toewijzingsdetails zijn aangescherpt om beter overeen te stemmen met Territory Helper.

Als u niet langer alle kaarten voor uw zaal wilt zien, moet u:
  1. Wijzig de instellingen van uw Zaal-uitgever in Territory Helper
  2. "Rechten vernieuwen" in het scherm met Territory instellingen in ServicePlanner

Raadpleeg de Help-documenten om de rechten beter te begrijpen:
https://serviceplanner.me/help/  
  
---  
  
Positieve beoordelingen in de App Store zijn ongelooflijk handig. Als u van ServicePlanner geniet en u bent geïnspireerd om een reactie achter te laten, zou dit zeer op prijs worden gesteld! Bedankt voor je steun :)

Website: serviceplanner.me  
Help Docs: serviceplanner.me/help  
Issue Tracker: serviceplanner.me/issues  
Facebook: facebook.com/ServicePlannerApp
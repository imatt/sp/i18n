Verschillende bugfixes en verbeteringen.

Zie de website voor meer informatie.

---

  
Positieve beoordelingen in de App Store zijn ongelooflijk handig. Als u van ServicePlanner geniet en u bent geïnspireerd om een reactie achter te laten, zou dit zeer op prijs worden gesteld! Bedankt voor je steun. :)

Website: serviceplanner.me
Help Documenten: serviceplanner.me/help
Probleem Oplosser (Engels): serviceplanner.me/issues
Facebook: facebook.com/ServicePlannerApp
* Integratie met  Territory Helper (www.territoryhelper.com)

* Nieuwe schermindeling voor vandaag:
  - Kies welke plug-ins u wilt inschakelen
  - Beheer de volgorde van plug-ins
  - Nieuwe activiteitsplugin
  - Nieuwe uren overzicht Plugin
  
* Ondersteuning voor meerdere getuigenpartners
  - Met iOS Kalender-integratie
  
* Betere integratie van Google Maps
  
* Verbeteringen voor videoweergave
  
* Mogelijkheid om 2x telefoonnummers per contact toe te voegen
  
* Mogelijkheid om kanalen, video's, bezoeken uit je rapport uit te sluiten (maar laat het contact achter)
  
* Mogelijkheid toegevoegd om rapporten via LINE te verzenden
  
* Diverse verbeteringen in de gebruikersinterface en problemen opgelost
  
U wordt aangemoedigd om over deze nieuwe functies op de site te lezen: https://serviceplanner.me/blog/
  
---
  
Positieve beoordelingen in de App Store zijn ongelooflijk handig. Als u van ServicePlanner geniet en u bent geïnspireerd om een reactie achter te laten, zou dit zeer op prijs worden gesteld! Bedankt voor je steun :)

Website: serviceplanner.me  
Help Documenten: serviceplanner.me/help  
Probleem Oplosser (Engels): serviceplanner.me/issues  
Facebook: facebook.com/ServicePlannerApp
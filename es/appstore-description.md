ServicePlanner es una aplicación para el Ministerio del Campo y la Administración de Contactos del desarrollador de Equipd. 

Diseñado para los testigos de Jehová, ServicePlanner combina características de gran alcance con una interfaz intuitiva y atractiva. El objetivo no es solo contar el tiempo, sino hacer que tu tiempo cuente. ¡Programe y planifique su ministerio a partir de hoy!

* Dropbox Sync - copia de seguridad automática de sus datos y sincronización a través de varios dispositivos.

* Pantalla Hoy - acceso rápido a videos, publicaciones y a sus llamadas planificadas para hoy. 

* Temporizador - registre su tiempo pasado en el ministerio o en "otro" trabajo (LDC etc) usando el temporizador. Inicio / Pausa / Reanudar / Detener el temporizador desde el icono de la aplicación utilizando Force Touch.

* Gestión de contactos - administre de forma centralizada sus llamadas desde una lista de contactos con capacidad de búsqueda. Los colores se usan cuidadosamente para indicar el tipo de contacto y si sus visitas están atrasadas o según lo programado. Deslice un contacto para volver a programar rápidamente su próxima visita, o agregar una visita de regreso. Toque + mantenga presionado para entrar en el modo de selección múltiple y editar varios contactos a la vez.

* Historial de visitas & Biblioteca - acceda rápidamente al historial de llamadas con una lista de visitas anteriores. De nuevo se utiliza el color para indicar visualmente el tipo de visita. Defina la fecha y los planes de la próxima visita. Una biblioteca central para cada contacto muestra todas las publicaciones que le haya colocado.

* Filtrado de contactos - el filtrado de gran alcance le permite definir cómo agrupar y clasificar sus llamadas. Busque Contactos usando Tags, Idioma, Localidad, Fecha de Próxima Visita e incluso Publicaciones Colocadas o No Colocadas.

* Vista de Mapa: obtenga una visión general de sus llamadas, busque direcciones, coloque marcadores personalizados para la ubicación de su automóvil, territorio, salón del Reino, su hogar y más. Puede pulsar + mantener para crear un nuevo contacto directamente desde una ubicación del mapa.

* Biblioteca - gestione de forma centralizada las Publicaciones y Videos que va a colocar con sus llamadas. Agregué una portada para identificar visualmente las publicaciones y vídeos. Puede introducir ubicaciones de vídeo para poder descargarlas para uso sin conexión o para transmitir en directo y liberar espacio de almacenamiento en el dispositivo. Añade elementos a tu pantalla Hoy.

* Actividad mensual: planifique su actividad diaria con Horas Planificadas versus Horas Reales. Vea una instantánea del progreso de su informe. Las horas proyectadas indican si usted está en camino de alcanzar su meta durante el mes, y si usted se mantiene en el horario planificado.

* Año de servicio - vea en un vistazo sus horas por mes y un resumen de su actividad para el año de servicio. Horas Proyectadas indican si está en el objetivo de alcanzar su objetivo de horas para el año.

* Programación: defina sus objetivos anuales, mensuales y diarios. A continuación, se utilizarán para realizar un seguimiento de su progreso para el mes y el año. Aplique su Programación Diaria a su Mes y vea las Horas Proyectadas - tan fácil de ver si alcanzara su meta o si necesita cambiar su horario.

* Informes - Soporte completo para Créditos de varias Horas y Traspasar Minutos. Su informe mensual se puede enviar por correo electrónico, TXT, Telegrama o WhatsApp. Configure los detalles del  superintendente y las opciones de informe en Configuración.

* "Otras Horas" - registre sus horas no ministeriales como trabajo de LDC o de Consulta de Sucursal. Éstos se pueden programar y planear junto con sus Horas del Ministerio. Configure el nombre de la categoría (por ejemplo, "LDC") y las opciones de informes en Configuración.

---

Eso es sólo una visión general rápida de las características actuales de Service Planner en la versión 1.0. Es sólo el comienzo - hay muchas más características planificadas.

Le recomiendo encarecidamente que consulte el sitio web que detalla las características y muestra ejemplos de capturas de pantalla: www.serviceplanner.me

¡Disfrútelo!

Requisitos: iOS 10+
Optimizado para iPhone y iPad.
El UI está actualmente solo en Ingles y  Alemán.
Soporte se proporciona en inglés, pero intentaré en otros idiomas.
Mapa tiene algún caché sin conexión, pero en general requiere un dispositivo conectado.
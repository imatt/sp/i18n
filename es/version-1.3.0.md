Por favor, para obtener más detalles acerca de esta actualización, consulte el sitio web: https://serviceplanner.me/blog

*** Testigos acompañantes ***  
En el Calendario de Actividad, ahora puede seleccionar un acompañante para cada día. Toque su nombre para llamarlos o enviarles mensajes. Utilice el conmutador para indicar si es un arreglo "Confirmado".

*** Recordatorio informe del mes ***  
Ahora recibirá un recordatorio para enviar su informe de servicio de campo. Puede personalizar este recordatorio en Ajustes> Notificaciones. 

*** Recordatorio visitas ***  
Ahora recibirá un recordatorio sobre las próximas visitas que ha programado. Puede personalizar este recordatorio en Ajustes> Notificaciones.  

*** Globos de notificación en el icono de la App ***  
El icono de la aplicación mostrará ahora un globo de notificación con un "1" rojo para indicar que el temporizador está funcionando. Esto se puede desactivar en Ajustes> Temporizador.

*** Importar contactos del dispositivo ***  
Si presiona + en el botón "+" de la pestaña Contactos, aparece la opción "Importar desde los contactos del dispositivo". Este es un importador básico que le permite extraer los datos de contactos existentes de su Libreta de direcciones. Consulte el sitio web para obtener detalles completos sobre cómo funciona.

*** Enviar mensajes a Contactos ***
Si el contacto tiene un número de teléfono, el botón "mensaje" permitirá ahora que los mensajes se envíen a través de SMS, Telegram, WhatsApp, Viber y otras aplicaciones, dependiendo de las aplicaciones que tenga instaladas en su dispositivo.

*** Sincronización en segundo plano ***  
Cuando la aplicación entra en segundo plano, ahora realizará un solo "empuje" de cualquier cambio realizado en su dispositivo local hacia Dropbox. Esto se activará si presiona el botón Inicio mientras utiliza ServicePlanner, o cambia de aplicaciones, o apaga el dispositivo. Esto garantizará que no pierda ningún cambio y que los datos sean sincronizados entre dispositivos.

*** Mejora en el soporte de zona horaria *** 
Algunos usuarios en lugares como Paraguay estaban experimentando varios problemas. Estos ya han sido resueltos.

*** Portugués ***  
¡ServicePlanner ahora está traducido para los usuarios portugueses (PT)! Si desea colaborar en traducir la aplicación a su idioma, por favor, regístrese en el Seguimiento de problemas (ver más abajo).

---

Espero que esta actualización sea del agrado de todo el mundo, y que usted esté disfrutando de ServicePlanner.

Las revisiones positivas en la App Store son increíblemente útiles. Si está disfrutando de ServicePlanner, y se siente inspirado para dejar un comentario, sería muy apreciado. Gracias por su apoyo.

Website: www.serviceplanner.me 
Documentos de ayuda: www.serviceplanner.me/help 
Últimas Noticias: www.serviceplanner.me/blog 
Seguimiento de Problemas: www.serviceplanner.me/issues
Facebook: www.facebook.com/ServicePlannerApp
* Integración con el ayudante Territorio (www.territoryhelper.com)

* Nuevo Diseño de Complementos en pantalla 'Hoy'
  - Elegir los complementos habilitados
  - Controlar el orden de complementos
  - Nuevo Complemento actividad
  - Nuevo Complemento Resumen Horario 

* Soporte para Múltiples compañeros Testigos
  - Con la integración Calendario iOS

* Mejora de la integración de Google Maps

* Mejoras en la Reproducción de vídeos

* Posibilidad de añadir 2x números de teléfono a un contacto

* Capacidad para eliminar ubicaciones, vídeos, visitas de su informe (pero dejar asociado al contacto)

* Mejoras en la interfaz de usuario múltiple y problemas resueltos

Se le anima a leer sobre estas nuevas características en el sitio web:
https://serviceplanner.me/blog/

---

Las revisiones positivas en la App Store son increíblemente útiles. Si está disfrutando de ServicePlanner, y se siente inspirado para dejar un comentario, sería muy apreciado. Gracias por su apoyo.

Website: www.serviceplanner.me 
Documentos de ayuda: serviceplanner.me/help 
Seguimiento de Problemas: serviceplanner.me/issues
Facebook: facebook.com/ServicePlannerApp
Thank you for helping to Localise and Translate the [ServicePlanner](https://serviceplanner.me) app. 

Please see the [Wiki](https://gitlab.com/imatt/sp/i18n/wikis/home) for instructions.
Forbedringer og feilrettinger.

Vennligst se nettstedets blogg for detaljer: www.serviceplanner.me/blog

---

Positive anmeldelser i App Store er utrolig nyttige. Hvis du liker ServicePlanner, og føler deg inspirert til å legge inn en anmeldelse, vil det bli satt stor pris på. Takk for støtten.

Nettsted: www.serviceplanner.me
Hjelp Dokumenter: www.serviceplanner.me/help
Siste nytt: www.serviceplanner.me/blog
Problemsporing: www.serviceplanner.me/issues
Facebook: www.facebook.com/ServicePlannerApp
Twitter: www.twitter.com/ServicePlanner_
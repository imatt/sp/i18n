Verbesserungen der Anzeigeregeln der Predigtdienst-Gebiete und deren Zuteilung in Zusammenarbeit mit Territory Helper.

Wenn du ab sofort nicht mehr alle Gebiete deiner Versammlung sehen kannst, musst du:

  1. Deine Verkündiger-Einstellungen in Teritorry Helper ändern
  2. "Erneuere Erlaubnis" in den Einstellungen für dein Versammlungsgebiet in ServicePlanner nutzen

Um die neue Regelung besser zu verstehen, besuche bitte die Hilfe-Seite auf: 
https://serviceplanner.me/help/  
  
---  
  
Positive Rezensionen im App Store sind extrem hilfreich. Wenn du Gefallen an der App hast, hinterlasse doch bitte eine kurze Rezension im App Store. Vielen Dank für deine Unterstützung!

Website: serviceplanner.me  
Help Docs: serviceplanner.me/help  
Issue Tracker: serviceplanner.me/issues  
Facebook: facebook.com/ServicePlannerApp
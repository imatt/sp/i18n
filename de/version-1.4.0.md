Neuer In-App Browser

- Von der Toolbar im Startbildschirm und in der Bibliothek aufrufbar.
- Funktioniert sehr ähnlich wie Safari.
- Tippe etwas ein, um mit Google danach zu suchen.
- Tippe eine Webseitenadresse ein, um die Webseite direkt aufzurufen. 
- Speichere deine Favoriten als Lesezeichen.
- Lesezeichen können in einer Kachel- oder Listenansicht angezeigt werden.
- Wie in vielen Browsern wird das Webseitenicon automatisch erkannt.
- Um das Lesezeichen in der Kachelansicht zu ändern, halte und drücke das Lesezeichenicon.
- In der Listenansicht können die Lesezeichen neu angeordnet werden.
- Unterstützte Videos werden automatisch erkannt und können direkt in die Bibliothek gespeichert werden.
- Reader-Ansicht verfügbar wie in Safari oder Firefox.
- Die Reader-Ansicht hat Themes wie einen Nachtmodus und unterstützt verschiedene Schriftarten.
- Die Reader-Ansicht kann per Mail verschickt werden, genauso wie in Safari.. 
- Die Reader-Ansicht unterstützt Chinese Pinyin, Zhuyin, Yale, Sidney Lau über Chinesischen Schriftzeichen, wenn der Inhalt der offiziellen Webseite gelesen wird. Klasse für alle die, die chinesische Sprachen lernen!

Im Startbildschirm kann ab sofort oben rechts in sekundenschnelle der Aktivitätenbildschirm für den heutigen Tag aufgerufen werden.

Auf dem Aktivitätsbildschirm für den heutigen Tag gibt es nun +/- Buttons um Abgaben und Videos manuell hinzufügen zu können. Wenn + gehalten wird kann eine Anzahl eingegeben werden. Wenn - gehalten wird, wird der Wert auf Null gesetzt.

Einige Fehler und andere Verbesserungen sind ebenfalls in diesem Update zu finden. Mehr über dieses Update im Blog.

---

I hoffe dieses Update findet eure Zustimmung und ihr genießt den Predigtdienst mit ServicePlanner.

Positive Rezensionen im App Store sind extrem hilfreich. Wenn du Gefallen an der App hast, hinterlasse doch bitte eine kurze Rezension im App Store. Vielen Dank!

Website: www.serviceplanner.me
Hilfe: www.serviceplanner.me/help
Neuigkeiten: www.serviceplanner.me/blog
Issue Tracker: www.serviceplanner.me/issues
Facebook: www.facebook.com/ServicePlannerApp
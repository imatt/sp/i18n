Neue Möglichkeit Kontakte zu teilen
============================

* Beim Teilen eines Kontaktes wird der E-Mail jetzt eine Datei angehängt, die andere Nutzer von ServicePlanner direkt in die eigenen Kontakte importieren können.

* Du kannst nur die Datei (ohne die Zusammenfassung per E-Mail) teilen und sie per AirDrop oder Apps wie Telegram, WhatsApp, Viber etc versenden.


Neue manuelle Sicherung / Wiederherstellung
============================

Als Alternative zur Synchronisation über Dropbox, kannst du jetzt eine manuelle Sicherung erstellen. Sende die Sicherung per AirDrop zu einem anderen Gerät, oder behalte sie in deiner Cloud oder einer Dateiverwaltungs-App. Du kannst die Sicherung auch über die Standard-E-Mail-App versenden, wobei die Datei dann über den kostenlosen Mail Drop-Service 30 Tage lang automatisch gespeichert wird. Du hats die Wahl.

Um die Sicherung wiederherszustellen finde die Datei auf deinem Gerät und wähle "Öffnen in..." ServicePlanner.

ServicePlanner erinnert dich optional auch an deine Sicherung: Wahlweise alle 7, 14, 30 oder 60 Tage.

Du kannst im ServicePlanner-Blog mehr über diese neue Funktion nachlesen.


---

Positive Rezensionen im App Store sind extrem hilfreich. Wenn du Gefallen an der App hast, hinterlasse doch bitte eine kurze Rezension im App Store. Vielen Dank!

Website: www.serviceplanner.me
Hilfe: serviceplanner.me/help
Issue Tracker: serviceplanner.me/issues
Facebook: facebook.com/ServicePlannerApp
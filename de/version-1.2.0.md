Die vollständige Update-Beschreibung mit Screenshots und Videos findest du hier: https://serviceplanner.me/blog

In diesem Update gibt es einiges Neues:


Detaillierte Berichtsübersicht
======================  

Unter Aktivitäten kann nun jede Berichtszeile der Berichtszusammenfassung angetippt werden.

Wenn du zum Beispiel auf "Abgaben" tippst erhälst du eine vollständige Übersicht deiner Abgaben pro Tag. In dieser Übersicht kannst du wiederum klicken, um direkt zum Kontakt oder dem Besuch der Abgabe zu gelangen.

Das ermöglicht ein deutlich klareres Verständnis deiner Diensttätigkeiten und wie diese Zusammenfassung des Monats berechnet wurde.


Kontaktmarkierung mittels Drag & Drop auf der Karte
=======================================

Du kannst jetzt auf einen Kontaktmarker auf der Karte tippen, um in den "Bewegungsmodus" zu kommen, mit dem du die Markierung einer neuen Position auf der Karte zuweisen kannst.

Tippe einfach woanders hin, um die neue Position des Kontaktes zu speichern.

Die Positionslokalisierung wurde verbessert, speziell für Länder, wo Adressen ungenau oder weniger hilfreich sind. Siehe den Link oben für mehr Informationen.


Benutzerdefinierte Kontaktfilter
=====================

Du kannst nun deine bevorzugten Filtereinstellungen speichern, um sie jederzeit wieder anwenden zu können. Tippe dafür auf "Als Schnellfilter speichern" ganz am Ende der Filtereinstellungen unter "Aktuell".

Du kannst nun auch die voreingestellten Filter ändern. Du kannst sie editieren, umbenennen und löschen. Außerdem kannst du die Reihenfolge deiner Filter ändern; der oberste Filter wird automatisch zu deinem "Standard"-Filter.

Tippe und halte auf das Filter-Symbol links oben in der Übersicht deiner Kontakte, um schnell auf deinen "Standard"-Filter zurückzusetzen.


Videos
=======

Gezeigte Videos können nun innerhalb eines Kontaktes übersichtlich angezeigt werden. Du kannst auf einen Blick sehen, welche Abgaben oder welche Videos du deinem Kontakt bereits abgegeben oder gezeigt hast.

Du kannst sogar nach Videos filtern. Das ist eine extrem nützliche Funktion - zum Beispiel, um alle Kontakte aufzulisten, denen du ein bestimmtes Video noch nicht gezeigt hast.


Aktivitätsnotizen
===============

Du kannst jetzt für deine täglichen Aktivitäten eine Notiz hinzufügen, zum Beispiel um zu erklären für welche LDC-Tätigkeiten diese Stunden genau sind oder andere nützliche Notizen, an die du dich für diesen Tag erinnern willst. Diese Notizen findest du direkt unter der Stundenanzeige in der Tagesübersicht deiner Aktivitäten.

Eine Monatszusammenfassung dieser Notizen können in der Stundenübersicht angezeigt werden.

Die Notizen können optional sogar deinem Bericht, den du verschickt, hinzugefügt werden. Dies kannst du unter Einstellungen aktivieren.


Verbesserungen in der Besuchsübersicht
===============

In der Übersichtsliste der Besuche eines Kontaktes werden jetzt alle Abgaben und Videos genauer benannt.

Beispiel: Bisher wurde dir hier nur angezeigt: "1 x Erwachet!", ab sofort steht dort "Erwachet! Nr. 3 (Juni) - Stammt die Bibel wirklich von Gott?".


Weitere Verbesserungen
===================

Viele weitere Verbesserungen und Anpassungen wurden vorgenommen.

Zum Beispiel kannst du jetzt unter Einstellungen->Bericht wählen, dass einzelne Berichtszeilen ausgeblendet werden, falls sie im Bericht mit 0 aufgeführt werden würden. Wenn du zum Beispiel keine Bibelstudien in einem Monat hattest, wird dies auf dem Bericht mit dieser Einstellungen nicht mehr aufgeführt.

Für Abgaben in der "Andere"-Kategorie kann nun vorgegeben werden, ob diese als Abgabe zum Bericht hinzugefügt werden oder nicht. Das kann für Abgaben nützlich sein, die du jemanden gibst, aber nicht als Abgabe berichten möchtest - zum Beispiel Kontaktkarten.

Außerdem ist die App nun komplett in Spanisch und Italienisch verfügbar! Danke an die so hart arbeitenden Übersetzer! Weitere Sprachen befinden sich bereits in der Übersetzung. Wenn du gerne bei der Übersetzung in deine Sprache mithelfen willst, melde dich bitte über den Issue Tracker (Link siehe unten).

---

Ich hoffe diese vielen Verbesserungen helfen dir in deinem Dienst noch effektiver zu sein und dabei das Maximum aus ServicePlanner herauszuholen.

Positive Rezensionen im App Store sind extrem hilfreich. Wenn du Gefallen an der App hast, hinterlasse doch bitte eine kurze Rezension im App Store. Vielen Dank!

---

Website: www.serviceplanner.me  
Hilfe: www.serviceplanner.me/help  
Neuigkeiten: www.serviceplanner.me/blog   
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp  
[v1.3.1] Fehlerbehebungen und Verbesserungen.

--- 

[v1.3.0] Mehr Informationen zu diesem Update sind auf dieser Webseite zu finden: https://serviceplanner.me/blog

*** Dienstpartner ***  
Im Aktivitätskalender kannst du jetzt für jeden Tag deinen Dienstpartner hinzufügen. Tippe auf seinen Namen und du kannst ihn direkt anrufen oder eine Nachricht schreiben. Nutze den Umschaltknopf, um deine Verabredung als "Bestätigt" festzulegen.

*** Berichtserinnerung ***  
Du erhälst jetzt eine Erinnerung, um deinen Predigtdienstbericht zu versenden. Du kannst diese Erinnerung unter Einstellungen > Benachrichtigungen anpassen.

*** Rückbesuchserinnerung ***  
Du erhälst auch eine Erinnerung über deine kommenden geplanten Besuche. Du kannst diese Erinnerung unter Einstellungen > Benachrichtigungen anpassen.  

*** App Badge Anzeige ***  
Das App Logo zeigt jetzt eine rote "1" während die Stoppuhr läuft. Das kannst du unter Einstellungen > Stoppuhr deaktivieren.

*** Importiere Kontakte ***  
Wenn du den "+"-Button in der Kontaktliste drückst und hälst kannst du einen Kontakt aus deinen persönlichen Adressbuch importieren und deine dort gespeicherten Kontaktdetails einfach zu übernehmen. Siehe obige Webseite für mehr Details.

*** Nachrichten via Messenger ***
Wenn bei einem Kontakt eine Telefonnummer hinterlegt ist, ermöglicht dir der "Nachrichten"-Button jetzt auch Nachrichten via Telegram, WhatsApp, Viber und anderen Apps zu verschicken - je nachdem welche du auf deinem Gerät installiert hast.

*** Hintergrund-Synchronisation ***  
Wenn die App in den Hintergrund geschlossen wird, werden jetzt alle Änderungen, die auf dem Gerät gemacht wurden mit einem einmaligen "Push" in deine Dropbox gespeichert. Das stellt sicher, dass wenn du den Homebutton drückst während du ServicepPlanner nutzt, die App wechselst oder dein Gerät ausschaltest, keine Daten verloren gehen und sie zwischen deinen Geräten immer aktuell sind.

*** Verbesserte Zeitzonenunterstützung ***  
Manche Nutzer in Ländern wie Paraguay haben Fehler in Verbindung mit der Zeitzone bemerkt. Diese wurden gelöst.

*** Portugiesisch ***  
ServicePlanner ist jetzt komplett auf Portugiesisch (PT) übersetzt! Wenn du beim Übersetzen der App in deine Sprache mithelfen willst, melde dich gerne über den Issue Tracker (Link siehe unten).

---

I hoffe dieses Update findet eure Zustimmung und ihr genießt den Predigtdienst mit ServicePlanner.

Positive Rezensionen im App Store sind extrem hilfreich. Wenn du Gefallen an der App hast, hinterlasse doch bitte eine kurze Rezension im App Store. Vielen Dank!

Website: www.serviceplanner.me  
Hilfe: www.serviceplanner.me/help  
Neuigkeiten: www.serviceplanner.me/blog  
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
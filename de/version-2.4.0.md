• Neuer DSGVO-Modus für Verkündiger in Großbritannien und der EU.

• Territory Helper Integration geändert, um den neuesten Datenschutzverordnungen zu entsprechen.
  
• Die App funktioniert jetzt auch für koreanische Verkündiger!

• Fehlerbehebungen und App-Verbesserungen.

Bitte schaue auf die Webseite für mehr Informationen: www.serviceplanner.me/blog

Positive Rezensionen im App Store sind extrem hilfreich. Wenn du Gefallen an der App hast, hinterlasse doch bitte eine kurze Rezension im App Store. Vielen Dank!

---

Website: www.serviceplanner.me  
Help Docs: www.serviceplanner.me/help  
Latest News: www.serviceplanner.me/blog
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
Twitter: www.twitter.com/ServicePlanner_
ServicePlanner ist eine Predigtdienst- und Kontaktmanagement-App vom Entwickler von Equipd.

Für Jehovas Zeugen entworfen, kombiniert ServicePlanner mächtige Funktionen mit einem intuitiven und attraktiven Interface. Zähle nicht nur die Zeit, sondern lass die Zeit für dich zählen: Organisiere und plane von heute an deinen Predigtdienst!

* Dropbox Synchronisation - Sichere automatisch deine Daten und synchronisiere sie mit deinen anderen Geräten.

* Startbildschirm - Schnellzugang zu Videos, Literatur und deine für heute geplanten Rückbesuche.

* Stoppuhr - Zeichne deine Dienstzeit oder Zeit für "andere Arbeiten" wie LDC mit der Stoppuhr auf. Unterstützung für Start/Pause/Stopp per Force Touch direkt auf dem App-Logo.

* Kontaktmanagement - Beherrsche deine Rückbesuche mit einer zentralen Kontaktliste. Farben helfen dir die Art des Kontaktes auf einen Blick zu identifizieren und überfällige Rückbesuche von anderen zu unterscheiden. Wische über einen Kontakt um schnell den nächsten Besuch einzuplanen oder um einen neuen Besuch hinzuzufügen. Drücke+Halte für eine Mehrfachauswahl und ändere mehrere Kontakte auf einmal.

* Rückbesuchshistorie und Bibliothek - Erreiche dank der Rückbesuchsliste eine Übersicht über alle vergangenen Rückbesuche. Lege das nächste Besuchsdatum fest und was du sagen willst. Lasse dir alle bereits abgegebenen Veröffentlichungen anzeigen.

* Kontakte filtern - Mächtige Filteroptionen ermöglichen es dir selbst zu definieren, wie du deine Kontakte sortieren möchtest. Durchsuche deine Kontakte mit Hilfe von Tags, nach Sprache, dem Wohnort, dem nächsten Besuchsdatum und sogar nach abgegebenen Veröffentlichungen.

* Kartenansicht - Erhalte eine Übersicht über deine Kontakte auf einer Landkarte, suche nach Adressen, platziere persönliche Markierungen zb dein Auto, deine Gebiete, dein Königreichssaal, deinem Zuhause und mehr.

* Bibliothek - Organisiere Veröffentlichungen und Videos, die du bei deinen Besuchen abgeben willst. Füge Cover hinzu, um die Veröffentlichungen einfach zu unterscheiden. Videos können für die Offline-Verwendung heruntergeladen oder direkt gestreamt werden, um Speicherplatz zu sparen. Füge Veröffentlichungen und Videos zu deinem Startbildschirm hinzu.

* Monatsplanungen - Plane deinen täglichen Dienst mit Hilfe der Übersicht aus aktuellen Stunden und in Zukunft eingeplanten Stunden. Sieh deinen aktuellen Fortschritt. Eingeplante Stunden zeigen dir, ob du aktuell mit deiner Zeit auf dem Laufenden bist und du dein Ziel erreichen wirst.

* Dienstjahresüberblick - Sieh auf einen Blick deine Stunden pro Monat und eine Zusammenfassung deiner Aktivität im gesamten Dienstjahr. Plane deine Stunden und verfolge, ob du mit deiner Zeit auf dem Laufenden bist, um dein Jahresziel zu erreichen. 

* Dienstplan - Definiere deine Stundenziele für das Jahr, den Monat und den einzelnen Tag, um deinen Fortschritt anzuzeigen. Trage deine persönlichen Dienstpläne ein und lass dir eine Übersicht des Monats anzeigen - das macht es so einfach zu sehen, ob du deine Zeit erreichen wirst oder deine Pläne anpassen musst.

* Bericht - Volle Unterstützung von Stundengutschriften und die Möglichkeit Minuten in den nächsten Monat mitzunehmen. Deinen Bericht kannst du per E-Mail, SMS, Telegram oder Whatsapp übermitteln. Gib dein Jahresziel als Verkündiger und vieles mehr in den Einstellungen vor.

* "Andere Stunden" - Notiere deine Nicht-Predigtdienst-Stunden wie Tätigkeiten für das LDC. Diese können neben deinen normalen Stunden eingepflegt werden. Vergib eigene Kategorien (zb "LDC").

---

ServicePlanner bietet eine englische und deutsche Übersetzung.

Ich empfehle die Webseite www.serviceplanner.me für mehr Informationen und Screenshots.

Viel Spaß mit ServicePlanner!

Anforderungen: iOS 10+
Optimiert für iPhone und iPad.
Hilfe und Support wird in englisch angeboten, aber ich versuche auch gerne in anderen Sprachen zu helfen.
Kartendarstellung und Positionserfassung benötigen eine Internetverbindung, auch wenn es eine einfache Offline-Funktion gibt.
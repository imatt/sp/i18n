Fehlerbehebungen und Verbesserungen.

Mehr Informationen zu diesem Update sind auf dieser Webseite zu finden: https://serviceplanner.me/blog

---

Positive Rezensionen im App Store sind extrem hilfreich. Wenn du Gefallen an der App hast, hinterlasse doch bitte eine kurze Rezension im App Store. Vielen Dank!

Website: www.serviceplanner.me  
Hilfe: www.serviceplanner.me/help  
Neuigkeiten: www.serviceplanner.me/blog  
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
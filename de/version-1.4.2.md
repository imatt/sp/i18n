Ich freue mich, dass ServicePlanner ab sofort für Niederländische, Rumänische und Japanische Nutzer verfügbar ist.

Vielen Dank an die hart arbeitenden Übersetzer! Wenn du mithelfen möchtest, dass ServicePlanner auch in deiner Sprache verfügbar ist schreib mir bitte eine Mail an support@serviceplanner.me.

Bis Bald, viel Freude mit ServicePlanner!

---

Positive Rezensionen im App Store sind überaus hilfreich. Wenn du ServicePlanner gerne nutzt und gerne eine Rezension schreiben möchtest, würde ich mich sehr freuen. Vielen Dank für die Unterstützung!

Website: www.serviceplanner.me  
Help Docs: www.serviceplanner.me/help  
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp 
Alle Neuerungen zu diesem Update sind auf der Webseite https://serviceplanner.me zu finden:

*** Stoppuhr ***  
Diese Funktion wurde auf vielfachen Wunsch integriert. Starten / Pausieren / Fortsetzen der Stoppuhr direkt vom Startbildschirm aus. Wenn die Stoppuhr pausiert ist, kann die aufgezeichnete Zeit als Predigtdienstzeit oder Zeit für andere Tätigkeiten zb LDC zum Bericht hinzugefügt werden. Die Stoppuhr zeichnet sogar weiter auf, wenn die App geschlossen oder dein Gerät neugestartet wird. In den Einstellungen kannst du die Farbe der Zeitleiste an deinen persönlichen Geschmack anpassen. Hier gibt es ausserdem eine Übersicht der bisher aufgezeichneten Zeiten. 

*** Unterstützung für 3D Touch ***  
Wenn dein Gerät Force Touch unterstützt, kannst du direkt über das App-Logo einen neuen Kontakt hinzufügen und die Stoppuhr bedienen.

*** Benutzerdefinierte Rückbesuchszeit ***  
Mit einem Wisch über einen Kontakt in der Kontaktliste (oder nach der Auswahl mehrerer Kontakte mit Drücken+Halten) kannst du das Rückbesuchsdatum jetzt blitzschnell festlegen. Neben den Auswahlmöglichkeit für Heute, Morgen, in einer Woche, in zwei Wochen etc kannst du jetzt ein benutzerdefiniertes Datum für den nächsten Besuch festlegen.

*** Split View Unterstützung ***  
Wenn dein iPad Split View und Slide Over unterstützt, kannst du diese tollen Funktionen ab sofort genießen.

*** Deutsche Übersetzung ***  
Während man Publikationen und Videos bereits bisher in jeder Sprache eingeben konnte, war die Bedienung der App (Erklärungstexte, Schaltflächen etc) nur auf englisch möglich. Dank dem begeisterten Einsatz eines Nutzers von ServicePlanner ist die App nun vollständig für deutsche Benutzer übersetzt! Wenn auch du dabei helfen willst die App in deine Sprache zu übersetzen, melde dich gerne bei mir oder über den Issue Tracker (siehe unten)

Nachdem die App erst vor gut einer Woche im App Store veröffentlicht wurde, sind das eine ganze Menge neue Funktionen und Verbesserungen in diesem Update! Vielen Dank für das postivie Feedback. Das ist erst der Anfang - es sind noch viel mehr tolle Funktionen für ServicePlanner geplant!

---

Positive Rezensionen im App Store sind unheimlich wertvoll. Wenn du Freude mit ServicePlanner hast, würde ich mich über eine Rezension sehr freuen. Danke für deine Unterstützung.

Webseite: www.serviceplanner.me  
Hilfe und Support: www.serviceplanner.me/help  
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
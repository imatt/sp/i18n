• Neue Funktion in der Jahresübersicht unter Aktivität, um den Jahresbericht manuel neu zu berechnen.

• Die App spricht jetzt norwegisch - Danke an John!

• Mehrere Verbesserungen und Fehlerbehebungen

Lies alles über die neuen Funktionen auf dieser Webseite:
https://serviceplanner.me/blog/

Positive Rezensionen im App Store sind extrem hilfreich. Wenn du Gefallen an der App hast, hinterlasse doch bitte eine kurze Rezension im App Store. Vielen Dank!

---

Website: www.serviceplanner.me  
Help Docs: www.serviceplanner.me/help  
Latest News: www.serviceplanner.me/blog
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
Twitter: www.twitter.com/ServicePlanner_
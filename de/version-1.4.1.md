Das ist ein kleines Update mit nur einer neuen Funktion...

In der Detailsanzeige eines Kontakts gibt es jetzt einen "Kontakt teilen" Button.

Das ist eine einfache Funktion, mit der man die Kontaktdetails und die gesamte Besuchshistorie per Mail versenden kann. Aktuell gibt es noch keine Möglichkeit, dass der Empfänger diese Informationen in ServicePlanner importieren kann. Diese Funktion kommt in der Zukunft. Damit du diese Funktion nutzen kannst muss die Standard Mail App funktionsfähig eingerichtet sein.

Bis Bald, viel Spaß mit ServicePlanner!

---

Positive Rezensionen im App Store sind extrem hilfreich. Wenn du Gefallen an der App hast, hinterlasse doch bitte eine kurze Rezension im App Store. Vielen Dank!

Website: www.serviceplanner.me
Hilfe: www.serviceplanner.me/help
Neuigkeiten: www.serviceplanner.me/blog
Issue Tracker: www.serviceplanner.me/issues
Facebook: www.facebook.com/ServicePlannerApp
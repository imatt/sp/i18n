Nouvelles options pour le partage de contact
============================

* Le partage par e-mail inclut désormais une pièce jointe qu'un autre utilisateur de ServicePlanner peut utiliser pour importer directement le contact.

* Il est possible également de partager le fichier via AirDrop ou via messagerie telle que Telegram, WhatsApp, Viber, etc.


Sauvegarde/Restauration manuelle
============================

Au lieu de Dropbox Sync, vous pouvez maintenant choisir de sauvegarder manuellement. Envoyez la sauvegarde via AirDrop à un autre appareil ou stockez-la à l'aide d'un service cloud ou d'une application de partage de fichiers. Vous pouvez également envoyer la sauvegarde par email via l'application Mail par défaut, qui utilisera la solution gratuite Mail Drop pour stocker le fichier pendant 30 jours. A vous pouvez choisir.

Pour restaurer la sauvegarde, recherchez simplement le fichier ZIP et choisissez "Ouvrir dans ..." ServicePlanner.

La fonction de sauvegarde manuelle inclut un service de rappel facultatif - choisissez d'être rappelé tous les 7, 14, 30 ou 60 jours.

Vous pouvez en savoir plus sur ces nouvelles fonctionnalités sur le blog de ServicePlanner, qui les explique plus en détail.


---

Les avis positifs dans l'App Store sont très précieux et utiles. Si vous aimez ServicePlanner, n'hésitez pas à laisser un commentaire, j'apprécie par avance. Merci de votre soutien.

Website: serviceplanner.me 
Help Docs: serviceplanner.me/help 
Issue Tracker: serviceplanner.me/issues 
Facebook: facebook.com/ServicePlannerApp
J'ai le plaisir de vous présenter la traduction française de ServicePlanner pour les utilisateurs francophones. Merci aux différents traducteurs qui ont oeuvré dessus.

Cette mise à jour inclut un certain nombre d'améliorations et de résolution de bugs. Consultez le site web pour plus de détails.

Notez que le support continue à être en anglais, mais que cela ne vous décourage pas de m'envoyer vos remarques, je reste à l'écoute et aiderai quand cela sera possible.

Les avis positifs dans l'App Store sont très précieux et utiles. Si vous aimez ServicePlanner, n'hésitez pas à laisser un commentaire, j'apprécie par avance. Merci de votre soutien.

---

Site Web: www.serviceplanner.me
Documents d'aide: www.serviceplanner.me/help
Dernières nouvelles: www.serviceplanner.me/blog
Suivi des problèmes: www.serviceplanner.me/issues
Facebook: www.facebook.com/ServicePlannerApp
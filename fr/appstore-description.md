Conçue pour les Témoins de Jéhovah, ServicePlanner est une application dédiée à la gestion des contacts et du ministère.

* Dropbox Sync - sauvegarde automatique des données sur tous vos appareils.

* Intégration avec Territory Helper.

* Ecran "Aujourd'hui" - accès rapide aux vidéos, publications et aux visites prévues pour la journée. 

* Chronomètre - enregistre le temps passé dans le ministère ou autre activité (LDC etc). Fonctions liées au chronomètre accessibles via ForceTouch sur l'icône de l'app.

* Gestion des contacts - gére de manière centralisée vos visites avec une liste de contacts. Les couleurs sont soigneusement utilisées pour indiquer le type de contact et si vos visites sont en retard ou programmées. 

* Historique et liste de visites - accédez rapidement à votre historique avec une liste des visites précédentes. La couleur est également utilisée pour indiquer visuellement le type de visite. Définissez votre prochaine date et sujet de visite. Une bibliothèque centrale pour chaque contact montre toutes les publications que vous avez placées avec eux.

* Filtrage des contacts - un filtre puissant vous permet de définir comment vous groupez et triez vos visites. Rechercher des contacts à l'aide de balises, de la langue, de la localité, de la prochaine date de visite et même des publications placées ou non placées.

* Carthographie - obtenez un aperçu de vos visites, recherchez des adresses, placez des marqueurs personnalisés pour l'emplacement de votre voiture, territoire, salle du Royaume, votre maison et plus encore. Vous pouvez appuyer sur + maintenir pour créer un nouveau contact directement à partir d'un emplacement sur la carte.

* Bibliothèque - gérez de manière centralisée les publications et vidéos que vous laissez. Ajoutez Cover Art pour identifier visuellement les publications et vidéos. Des emplacements vidéos peuvent être saisis pour vous permettre de télécharger pour une utilisation hors ligne, ou pour diffuser en direct et libérer de l'espace de stockage sur votre appareil. Ajouter des éléments à votre écran "Aujourd'hui".

* Activité mensuelle - planifiez votre activité quotidienne avec les heures prévues et les heures réelles. Afficher un instantané en direct de la progression de votre rapport. Les heures projetées indiquent si vous êtes sur la bonne voie avec votre temps au cours du mois et si vous restez dans les temps.

* Année de service - consultez d'un coup d'œil vos heures par mois et un résumé de votre activité pour l'année de service. Les heures projetées indiquent si vous êtes sur le bon cap pour atteindre votre objectif horaire annuel.

* Planification - définissez vos objectifs annuels, mensuels et quotidiens. Ceux-ci seront ensuite utilisés pour suivre vos progrès pour le mois et l'année. Appliquez votre horaire quotidien à votre mois et affichez les heures prévues - facile pour voir si vous aurez votre temps visé ou si vous avez besoin de modifier votre agenda.

* Création de rapports - prise en charge de multiples lignes de crédits d'heures et minutes. Votre rapport mensuel peut être soumis en utilisant Email, TXT, Telegram ou WhatsApp. Configurez les détails de vos responsables et les options de votre rapport dans Paramètres.

* «Autres heures» - consignez vos heures non liées au ministère, par exemple le travail des LDC ou liée aux filiales. Celles-ci peuvent être planifiées parallèlement à vos heures de ministère. Configurez le nom de la catégorie (par exemple "LDC") et les options de rapport dans les paramètres.
 
---

Ce n'est que le début - il y a tellement d'autres fonctionnalités prévues.

Version iOS minimum : iOS 10+  
Optimisée pour iPhone et iPad.

Le support est actuellement fourni en anglais principalement, mais je peux essayer dans d'autres langues.

La cartographie a une mise en cache hors ligne mais nécessite généralement un périphérique connecté à Internet.
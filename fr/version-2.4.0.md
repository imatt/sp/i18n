• Nouveau mode 'Respct RGDP' pour les utilisateurs situés en Europe et et UK.

• Intégration à Territory Helper revue afin d'intégrer les changements liés au RGPD.
  
• L'application est disponible en Coréen !

• Corrections de bugs et améliorations.

Veuillez consulter le site Web pour plus de détails sur cette mise à jour : www.serviceplanner.me/blog

Les avis positifs sur l'App Store sont incroyablement utiles. Si vous appréciez ServicePlanner et que vous vous sentez assez inspiré pour laisser un commentaire, ce serait grandement apprécié. Merci pour votre aide.

---

Website: www.serviceplanner.me  
Help Docs: www.serviceplanner.me/help  
Latest News: www.serviceplanner.me/blog
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
Twitter: www.twitter.com/ServicePlanner_
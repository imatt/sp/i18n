버그(Bug) 수정 및 기능 개선.

이 업데이트에 대한 상세한 내용는 웹사이트를 참고하시기 바랍니다.
www.serviceplanner.me/blog

앱 스토어에서 긍정적인 후기를 해주시면 엄청난 도움이 됩니다. ServicePlanner가 쓸만하다면 자유롭게 후기를 남겨 주시면 대단히 고맙겠습니니다. 여러분의 지원에 깊이 감사드립니다!

---

Website: www.serviceplanner.me  
Help Docs: www.serviceplanner.me/help  
Latest News: www.serviceplanner.me/blog
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp  
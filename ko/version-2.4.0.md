• 영국 및 EU 사용자를 위한 새로운 개인정보보호규정(GDPR) 준수 모드.

• Territory Helper 통합 기능이 최근의 개인정보보호규정(GDPR) 관련 변경 사항과 일치하도록 업데이트되었습니다.
  
• 앱은 이제 한국어 사용자를 지원합니다!

• 버그 수정 및 개선.

이 업데이트에 대한 상세한 내용는 웹사이트(www.serviceplanner.me/blog)를 참고하시기 바랍니다.

앱 스토어에서 긍정적인 후기를 해주시면 엄청난 도움이 됩니다. ServicePlanner가 쓸만하다면 자유롭게 후기를 남겨 주시면 대단히 고맙겠습니니다. 여러분의 지원에 깊이 감사드립니다!

---

Website: www.serviceplanner.me  
Help Docs: www.serviceplanner.me/help  
Latest News: www.serviceplanner.me/blog
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
Twitter: www.twitter.com/ServicePlanner_
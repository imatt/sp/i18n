ServicePlanner adalah aplikasi untuk Dinas Lapangan dan Manajemen Kontak dari pengembang Equipd. 

ServicePlanner dirancang bagi Saksi-Saksi Yehuwa dan menggabungkan banyak fitur yang bagus dengan tampilan yang menarik dan mudah dipelajari. Tujuannya bukan sekadar untuk menghitung jam dinas, tetapi membuat dinas Saudara produktif. Jadwalkan dan rencanakanlah dinas Saudara hari ini juga!

* Sinkronisasi Dropbox - otomatis melakukan backup data dan mensinkronkannya ke beberapa perangkat sekaligus.

* Layar Hari Ini - akses cepat ke Video, Publikasi dan rencana kunjungan Saudara hari ini. 

* Timer - catat waktu yang Saudara gunakan dalam dinas atau kegiatan "lain" (RBS, dsb.) dengan timer. Start/Pause/Resume/Stop timer dari ikon aplikasi dengan fitur Force Touch.

* Manajemen Kontak - atur Kunjungan Saudara dengan daftar kontak yang mudah dicari. Warna bisa digunakan untuk menunjukkan jenis Kontak dan apakah kunjungan Saudara perlu segera dikunjungi atau akan dikunjungi. Slide Kontak untuk menjadwalkan kunjungan berikutnya ke mereka. Tap+tahan untuk masuk ke mode multi-pilih dan edit beberapa kontak sekaligus.

* Riwayat Kunjungan Kembali & Pustaka - akses cepat ke semua kunjungan kembali berikut daftar kunjungan sebelumnya. Warna juga digunakan untuk menunjukkan jenis Kunjungan. Tentukan Tanggal Kunjungan Berikutnya dan Rencana. Pustaka terpusat untuk setiap kontak menunjukkan semua publikasi yang pernah ditempatkan kepadanya.

* Filter Kontak - filter yang sangat komplit sehingga Saudara bisa mengatur Pengelompokan dan Penyortiran semua kunjungan Saudara. Cari Kontak dengan Tag, Bahasa, Lokasi, Tanggal Kunjungan Berikut, dan bahkan Publikasi yang Sudah atau Belum Ditempatkan.

* Tampilan Peta - lihat secara keseluruhan semua Kunjungan Saudara, cari alamat, letakkan penanda untuk lokasi Kendaraan, Daerah, Balai Kerajaan, Rumah Saudara, dan banyak lagi. Saudara dapat melakukan tap+tahan untuk menciptakan Kontak baru langsung dari lokasi di peta.

* Pustaka - kelola Publikasi dan Video yang hendak Saudara tempatkan ke semua kunjungan. Tambahkan Gambar Sampul agar mudah mengenali Publikasi dan Video. Lokasi video dapat dimasukkan sehingga Saudara dapat mengunduh untuk digunakan tanpa Internet, atau tayangkan secara streaming sehingga menghemat tempat penyimpanan di perangkat Saudara. Tambahkan publikasi atau video ke layar Hari Ini.

* Kegiatan Bulanan - rencanakan kegiatan sehari-hari dengan Jam Rencana vs Jam Aktual. Lihat sejauh mana Laporan Saudara berjalan. Jam Proyeksi menunjukkan sejauh mana Saudara mencapai target jam untuk bulan itu, dan apakah Saudara masih bisa memenuhinya.

* Tahun Dinas - lihat sekilas jam Saudara per bulan, dan ringkasan kegiatan Saudara untuk Tahun Dinas itu. Jam Proyeksi menunjukkan apakah Saudara masih bisa mencapai Tujuan Jam untuk tahun itu. 

* Penjadwalan - tentukan Target Jam Tahunan, Bulanan, dan Harian Saudara. Informasi ini akan digunakan untuk mengetahui sejauh mana yang Sauara capai untuk Bulan dan Tahun bersangkutan. Gunakan Jadwal Harian Saudara pada Bulan Sauara dan lihat Jam Proyeksi - mudah sekali melihat apakah Saudara bisa mencapai kuota atau perlu menyesuaikan kembali jadwal Saudara.

* Laporan - mendukung sepenuhnya beberapa Kredit Jam dan Menit yang Dibawa ke Bulan Berikut. Laporan Bulanan Saudara dapat dikirimkan melalui Email, TXT, Telegram atau WhatsApp. Tetapkan perincian pengawas Saudara dan opsi laporan di bagian Pengaturan. 

* "Jam Lain" - catat jam yang bukan dalam dinas, seperti pekerjaan RBS atau Konsultan Cabang. Ini bisa dijadwalkan dan direncanakan bersama dengan Jam Dinas. Tentukan nama kategori (mis. "RBS") dan opsi Laporan di Pengaturan.

---

Itu hanya tinjauan singkat fitur-fitur ServicePlanner saat ini dalam versi 1.0. Ini barulah awal - ada banyak sekali fitur yang sedang direncanakan.

Saya anjurkan Saudara untuk memeriksa situs Web ini yang menjabarkan lebih terperinci setiap fitur berikut contoh tampilannya: www.serviceplanner.me

Selamat menggunakan!

Persyaratan: iOS 10+
Dapat digunakan di iPhone dan iPad.
Saat ini Tampilan Antarmuka hanya dalam bahasa Inggris dan Jerman.
Dukungan tersedia dalam bahasa Inggris, tetapi saya akan berupaya membantu dalam bahasa lain. 
Sebagian peta bisa disimpan secara offline, tetapi secara keseluruhan membutuhkan akses Internet.
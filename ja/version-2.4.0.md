• イギリスとEU圏のユーザーのため、GDPR (EU一般データ保護規則）を遵守するためのモードが追加されました。

• GDPRの変更に基づき、テリトリーヘルパーとの連携をアップデートしました。
  
• 韓国語がサポートされました。

• 軽微な不具合の修正を行いました。

Please see the website for full details on this update: www.serviceplanner.me/blog

アプリを気に入って頂けましたら、いいね評価とレビューをして頂けましたら大変助かります！


---

Website: www.serviceplanner.me  
Help Docs: www.serviceplanner.me/help  
Latest News: www.serviceplanner.me/blog
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
Twitter: www.twitter.com/ServicePlanner_
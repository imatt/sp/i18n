New Share Contact Options
============================

* Sharing using the Summary Email will now include a file attachment that another ServicePlanner user can use to directly import the Contact.

* You can share just the file (without the email) and send via AirDrop or using messaging apps like Telegram, WhatsApp, Viber etc.


New Manual Backup / Restore
============================

As an alternative to Dropbox Sync, you can now use the new Manual Backup option. Send the backup via AirDrop to another device, or store it using a cloud service or file sharing app. You can also email the backup via the default Mail app which will use the free Mail Drop solution to store the file for 30 days. The point is - you get to choose.

To restore the backup, simply locate the ZIP file and choose to "Open in..." ServicePlanner.

The Manual Backup feature includes an optional Reminder service - choose to be reminded every 7, 14, 30 or 60 days.

You can read about these new features on the ServicePlanner Blog which explains them in more detail.


---

Positive reviews on the App Store are incredibly helpful. If you are enjoying ServicePlanner and feel inspired to leave a review, it would be greatly appreciated! Thanks for your support :)

Website: serviceplanner.me 
Help Docs: serviceplanner.me/help 
Issue Tracker: serviceplanner.me/issues 
Facebook: facebook.com/ServicePlannerApp
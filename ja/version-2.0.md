*テリトリーヘルパー（www.territoryhelper.com）との統合

*今日の画面上の新しいプラグインのレイアウト
   - 有効にするプラグインの選択
   - プラグインの順序を制御する
   - 新しいアクティビティプラグイン
   - 新しい時刻サマリープラグイン
  
*複数の目撃者のためのサポート
   - iOSカレンダーとの統合
  
*改善されたGoogleマップの統合
  
*再生動画の改善
  
*連絡先ごとに2倍の電話番号を追加する機能
  
*プレースメント、動画、訪問のレポートを削除する機能（連絡先から離れる）
  
* LINE経由でレポートを送信する機能を追加
  
*複数のUIの改善と解決された問題
  
これらの新機能については、ウェブサイト（https://serviceplanner.me/blog/）でお読みください。
  
---
  
App Storeの積極的なレビューは信じられないほど役に立ちます。あなたがServicePlannerを楽しんでいて、レビューを残すように促す気があるなら、それは非常に高く評価されるでしょう！ご協力ありがとうございました ：）

ウェブサイト：www.serviceplanner.me
ヘルプドキュメント：serviceplanner.me/help
Issue Tracker：serviceplanner.me/issues
Facebook：facebook.com/ServicePlannerApp
改善とバグ修正。

詳細については、ウェブサイトのブログ（英語）をご覧ください：www.serviceplanner.me/blog

---

アプリを気に入って頂けましたら、いいね評価とレビューを書いて頂けると大変励みになります！

ウェブサイト：www.serviceplanner.me
ヘルプドキュメント：www.serviceplanner.me/help
最新ニュース：www.serviceplanner.me/blog
改善点トラッカー：www.serviceplanner.me/issues
Facebook：www.facebook.com/ServicePlannerApp
Twitter：www.twitter.com/ServicePlanner_

Correção de problemas e melhoramentos.

Incentivo-o a ler sobre estes novos recursos no site:
https://serviceplanner.me/blog/

---

Avaliações positivas na App Store são incrivelmente úteis. Se está a gostar do ServicePlanner, e se sentir inspirado para deixar um comentário, seria muito apreciado. Obrigado pelo seu apoio.

Website: www.serviceplanner.me
Documentos de Ajuda: serviceplanner.me/help
Registo de Problemas: serviceplanner.me/issues
Facebook: facebook.com/ServicePlannerApp
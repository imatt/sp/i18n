Novas opções de partilha de Contato
===================================

* Partilhar ao usar o Email Resumido agora irá incluir um ficheiro em anexo que outro usuário ServicePlanner pode usar para importar diretamente o contacto.

* Pode partilhar apenas o ficheiro (sem o email) e enviar via Airdrop ou por usar aplicações de mensagens como o Telegram, WhatsApp, Viber etc.


Nova Cópia de Segurança / Restauro Manual
===================================

Como alternativa ao Dropbox Sync, agora pode usar a nova opção de Cópia de Segurança manual. Envie a Cópia de Segurança via AirDrop para outro dispositivo ou armazene-a usando um serviço na nuvem ou uma aplicação de partilha de ficheiros. Também pode enviar a cópia de segurança por e-mail através da aplicação Mail padrão, que usará a solução Mail Drop gratuita para armazenar o arquivo por 30 dias. O ponto é - agora pode escolher.

Para efetuar o restauro da Cópia de Segurança, simplesmente localize o ficheiro ZIP e escolha "Abrir em..." ServicePlanner.

O recurso de Cópia de Segurança manual inclui um serviço de lembrete opcional - opte por ser lembrado a cada 7, 14, 30 ou 60 dias.

Pode ler sobre esses novos recursos no Blog do ServicePlanner, que os explica com mais detalhes.


---

Avaliações positivas na App Store são incrivelmente úteis. Se está a gostar do ServicePlanner, e se sentir inspirado para deixar um comentário, seria muito apreciado. Obrigado pelo seu apoio.

Website: www.serviceplanner.me
Documentos de Ajuda: www.serviceplanner.me/help
Registo de Problemas: www.serviceplanner.me/issues
Facebook: www.facebook.com/ServicePlannerApp

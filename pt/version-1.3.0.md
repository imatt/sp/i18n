Detalhes completos desta atualização podem ser encontradas em: https://serviceplanner.me/blog

*** Companheiro/a de Testemunho ***  
No calendário de Atividades, pode selecionar um companheiro/a para cada dia. Toque no nome para lhe telefonar ou enviar mensagem. Alterne para indicar se é um arranjo "confirmado".

*** Lembrete de Relatório ***  
Agora receberá um lembrete para enviar o seu relatório. Pode personalizar o lembrete em  Definições > Notificações.   

*** Lembrete de Visitas ***  
Agora receberá um lembrete para as próximas visitas agendadas. Pode personalizar o lembrete em Definições > Notificações.  

*** Emblema no Icon da Aplicação ***  
No Icon da aplicação agora pode ver a vermelho o numero "1" para indicar que o temporizador está a contar. Ista função pode ser desativado em Definições > Temporizador.   

*** Importar Contactos do Equipamento ***  
Se pressionar e mantiver o dedo no botão "+" no separador Visitas, vai abrir uma opção para "Importar Contactos do Equipamento". É um importador básico que permite obter os detalhes dos contactos do seu livro de endereços. Veja o site para obter mais detalhes de como funciona.

*** Mensagem para Contacto ***
Se o contacto tiver um numero de telefone, o botão "Enviar Mensagem" vai permitir enviar mensagens pelo Telegram, WhatsApp, Viber ou outras aplicaçõe - dependendo das aplicações que tem instaladas no seu equipamento.

*** Sincronização em segundo plano ***  
Quando a aplicação passar para segundo plano efetuará um unico pedido de sincronização com o Dropbox de qualquer alteração efetuada na aplicação. Isto vai acontecer nas seguintes situações: Quando estiver a usar a aplicação ServicePlanner e premir o botão "Home", ou mudar de aplicação, ou desligar o equipamento. Isto assegurará que não perderá nenhuma alteração efetuada e que as alterações serão sincronizadas entre todos os seus equipamentos. 

*** Suporte de Fuso Horário melhorado ***  
Alguns utilizadores em locais como o Paraguai experenciaram alguns problemas. Problemas estes resolvidos.

*** Português ***  
ServicePlanner foi traduzido para os utilizadores Portugueses (PT)! Se pretender ajudar na tradução da aplicação na sua lingua registe o seu interesse na página: "Issue Tracker" (ver abaixo).

---

Espero que esta atualização encontre todos bem, e que esteja a gostar do ServicePlanner.

Avaliações positivas na App Store são incrivelmente úteis. Se estiver a gostar do ServicePlanner e se sentir inspirado a deixar uma avaliação, seria muito apreciado. Obrigado pelo seu suporte.

Website: www.serviceplanner.me  
Documentos de Ajuda: www.serviceplanner.me/help  
Issue Tracker: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp
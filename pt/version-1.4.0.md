Novo Navegador incluido na aplicação: New in-app website browser: 

- Aceder a partir da barra de ferramentas superior no separador Hoje e Biblioteca.
- Funciona de forma similar ao Safari.
- Escreva uma frase de pesquisa para efetuar a pesquisa com o Google.
- Escreva um endereço URL para aceder diretamente ao site. 
- Marque os seus sites favoritos.
- Os Favoritos podem ser visualizados em grelha ou em lista.
- Similar a outros navegadores, os icons dos sites serão automáticamente detetados.
- Na vista em grelha toque e segure no favorito para o editar.
- Na vista em lista pode reordenar os seus favoritos.
- Os Videos suportados serão detetados e automáticamente guardados na aplicação.
- Disponivel o Modo de Leitura, semelhante ao Safari e ao Firefox.
- O Modo de Leitura possui alguns temas de visualização tais como o modo noturno, e suporta fontes personalizadas.
- O conteúdo do Modo de Leitura pode ser enviado por email, tal como no Safari. 
- O Modo de Leitura suporta o Chines Pinyin, Zhuyin, Yale, Sidney Lau acima dos carateres Chineses quando estiver a ler diretamente do site oficial JW.   

No topo superior direito no separador Hoje agora pode aceder rápidamente ao separador da atividade para o próprio dia.

No separador da Atividade Diária agora tem disponivel botões +/- para aumentar/diminuir o numero de colocações e videos mostrados. Toque e segure no "+" para inserir o numero manualmente. 

A correção de muitos erros e outros melhoramentos foram incluidos nesta atualização. Por favor leia mais sobre esta atualização no blog do site.

---

Espero que esta atualização encontre todos bem, e que estejam a gostar do ServicePlanner.

Avaliações positivas na App Store são incrivelmente úteis. Se está a gostar do ServicePlanner, e se sentir inspirado para deixar um comentário, seria muito apreciado. Obrigado pelo seu apoio.

Website: www.serviceplanner.me  
Documentos de Ajuda: www.serviceplanner.me/help  
Registo de Problemas: www.serviceplanner.me/issues  
Facebook: www.facebook.com/ServicePlannerApp 